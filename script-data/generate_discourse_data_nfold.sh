

## Script for generating data

data=$1
folder=$2
src=$3


echo $folder $data
mkdir -p $folder


## process_pdtb.py uses dep corpus to assign heads to const corpus, then transforms const corpus in new format
## arguments:
## 1. tbk (generate tbk file) or raw (keep only tokens)
## 2. constituent corpus (ptb style)
## 3. dep corpus (conll)
## output on stdout

## for train and dev datasets: generate tbk format and raw format
for c in nfold-1 nfold-2 nfold-3 nfold-4 nfold-5 nfold-6 nfold-7 nfold-8 nfold-9 nfold-10
do
    ## header of file must be the list of attributes for each token 
    echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.tbk
    
    python3 ${src}process_rst.py tbk ${data}nfold_const/${c}.brackets ${data}nfold_dep/${c}.conll ${data}nfold_parse/${c}.parse >> ${folder}/${c}.tbk
    
    echo word pos prefw1 prefw2 prefw3 prefp1 prefp2 prefp3 suffw1 suffp1 len headw1 headw2 headw3 headp1 headp2 headp3 head position numb perc money date> ${folder}/${c}.raw
    python3 ${src}process_rst.py raw ${data}nfold_const/${c}.brackets ${data}nfold_parse/${c}.parse >> ${folder}/${c}.raw
done
