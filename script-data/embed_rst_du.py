#!/usr/bin/python
# -*- coding: utf-8 -*-


import os, sys, shutil, argparse
import numpy as np
import random


'''
Write an embedding file where each EDU is represented by a vector
- Take the wd features used for RST expe, make a vector that is a concatenation of the vector representing each word

'''

FEATS = ['prefw1','prefw2', 'prefw3', 'headw1', 'headw2', 'headw3', 'suffw1']


def main( ):
    parser = argparse.ArgumentParser(
            description='Write an embedding file where each EDU is represented by a vector.')

    parser.add_argument('--rstdt',
            dest='rstdt',
            action='store',
            help='Directory containing the rstdt data')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Embedding file: one embed file per language, then one file for all')
    parser.add_argument('--wd_embed',
            dest='wd_embed',
            action='store',
            help='Embedding dir with embed for wd')
    parser.add_argument('--dimkept',
            dest='dimkept',
            action='store',
            default=50,
            help='Keep the first dimkept dim of the vectors for each word')
    parser.add_argument('--outunk',
            dest='outunk',
            action='store',
            help='Unknown words')
    '''parser.add_argument('--eduembed',
            dest='eduembed',
            action='store',
            nargs='+',
            help='Keep the first dimkept dim of the vectors for each word')'''
    args = parser.parse_args()

#     if not os.path.isdir( args.outpath ):
#         os.mkdir( args.outpath )
#     # Keep the first dim_kept dim of the vectors for each word
#     embed_files = embed( args.rstdt, args.outpath, args.wd_embed, dim_kept=int(args.dimkept) )
#     union( embed_files, args.outpath )

#     union_embed( args.eduembed, args.outpath )

    print( "!! Version with the first line of the embedding file ignored" )

    embed( args.rstdt, args.outpath, args.wd_embed, args.outunk, dim_kept=int(args.dimkept) )



def embed( rstdt, out_file, wd_embed, outunk, dim_kept=50 ):
    print( "Reading embeddings" )
    wd_vect = {}
    unk="-UNK-"

    for f in os.listdir( wd_embed ):
        if f.endswith( '.vec' ):
            wd_embed_lines = open( os.path.join( wd_embed, f ) ).readlines()
            #print( len(wd_embed_lines) )
            for l in wd_embed_lines[1:]:
                w, v = l.split()[0], [float(e) for e in l.split()[1:]][:dim_kept]
                if w == "<UNK>":
                    w = unk
                wd_vect[w] = v
    print( 'Vocab', len( wd_vect.keys() ) )
    if not "-UNK-" in wd_vect:
        unk_vect = np.average( list( wd_vect.values() ), axis=0 )
        wd_vect[unk] = unk_vect
        print( "Wd embeddings dim:", unk_vect.shape )



    # EDU --> vect
    embeddings = {}
    #for mydir in [d for d in os.listdir( rstdt ) if not d.startswith('.') and not d == "eu"]:#ignore Basque with Anders embed
    for mydir in [d for d in os.listdir( rstdt ) if not d.startswith('.')]:
        print( "\n-- Reading", mydir )
        print( "\n>Embed TRAIN" )
        if os.path.isfile( os.path.join( rstdt, mydir, "no-translation", "train.tbk" ) ):
            train = os.path.join( rstdt, mydir, "no-translation", "train.tbk" )
            print( "train", train )
            train_edus = read_tbk( train )
            unkwds_train = getEmbed( train_edus, embeddings, wd_vect, unk=unk )
        elif os.path.isfile( os.path.join( rstdt, mydir, "train.tbk" ) ):
            train = os.path.join( rstdt, mydir, "train.tbk" )
            print( "train", train )
            train_edus = read_tbk( train )
            unkwds_train = getEmbed( train_edus, embeddings, wd_vect, unk=unk )
        else:
            unkwds_train = set()
            print( "No train file" )
    #
        print( "\n>Embed DEV" )
        if os.path.isfile( os.path.join( rstdt, mydir, "no-translation", "dev.tbk" ) ):
            dev = os.path.join( rstdt, mydir, "no-translation", "dev.tbk" )
            dev_edus = read_tbk( dev )
            unkwds_dev = getEmbed( dev_edus, embeddings, wd_vect, unk=unk )
        elif os.path.isfile( os.path.join( rstdt, mydir, "dev.tbk" ) ):
            dev = os.path.join( rstdt, mydir, "dev.tbk" )
            dev_edus = read_tbk( dev )
            unkwds_dev = getEmbed( dev_edus, embeddings, wd_vect, unk=unk )
        else:
            print( "no dev file" )
#             sys.exit( "No dev file" )

        print( "\n>Embed TEST" )
        # For the test, only raw files
        if os.path.isfile( os.path.join( rstdt, mydir, "no-translation", "test.raw" ) ):
            test = os.path.join( rstdt, mydir, "no-translation", "test.raw" )
            test_edus = read_raw( test )
            unkwds_test = getEmbed( test_edus, embeddings, wd_vect, unk=unk )
        elif os.path.isfile( os.path.join( rstdt, mydir, "no-translation", "testa.raw" ) ):
            test = os.path.join( rstdt, mydir, "no-translation", "testa.raw" )
            test_edus = read_raw( test )
            unkwds_test = getEmbed( test_edus, embeddings, wd_vect, unk=unk )
        elif os.path.isfile( os.path.join( rstdt, mydir, "test.raw" ) ):
            test = os.path.join( rstdt, mydir, "test.raw" )
            test_edus = read_raw( test )
            getEmbed( test_edus, embeddings, wd_vect, unk=unk )
            unkwds_test = getEmbed( test_edus, embeddings, wd_vect, unk=unk )
        elif os.path.isfile( os.path.join( rstdt, mydir, "testa.raw" ) ):
            test = os.path.join( rstdt, mydir, "testa.raw" )
            test_edus = read_raw( test )
            getEmbed( test_edus, embeddings, wd_vect, unk=unk )


            unkwds_test = getEmbed( test_edus, embeddings, wd_vect, unk=unk )

        else:
            sys.exit( "No test file" )

    vocab_size = len( embeddings )
    dim_edu_embed = dim_kept*len(FEATS)


    o = open( out_file, 'w' )
    o.write( str( len( embeddings ) )+" "+str( dim_edu_embed )+"\n" )
    for e in embeddings:
        o.write( e+" "+" ".join( [str(v) for v in embeddings[e]] )+"\n" )
    o.close()

    unkwds = set.union(unkwds_train,unkwds_test,unkwds_dev)
    o = open( outunk, 'w' )
    for w in unkwds:
        o.write( w+"\n" )
    o.close()

    print( "\nEDU embeddings file:", out_file, "\nEDU embeddings dimensions:", dim_edu_embed, "\n\#EDUs", vocab_size, "\n#Unk Wds", len(unkwds) )

























# def union_embed( eduembed, outpath ):
#
#     lines = []
#     for f in eduembed:
#         flines = open( f ).readlines()[1:]
#         print( len( flines ) )
#         lines.extend( flines )
#     print( len( lines ) )
#     o = open( outpath, 'w' )
#     o.write( str(len(lines))+" "+"70\n" )
#     for l in lines:
#         o.write( l )
#     o.close()
#
# def union( embed_files, outpath ):
#     vocab = []
#     vectors= []
#     for f in embed_files:
#         print( "Reading:", f )
#         c = open( f )
#         lines = c.readlines()
#         c.close()
#         for l in lines[1:]:
#             l = l.strip()
#             edu, vec = l.split()[0], l.split()[1:]
#             if not edu in vocab:
#                 vocab.append( edu )
#                 vectors.append( " ".join( vec ) )
#
#     dim = len( vectors[0].split() )
#     o = open( os.path.join( outpath, 'edu_embeddings' ), 'w' )
#     o.write( str( len( vocab ) )+" "+str(dim)+"\n"  )
#     for i, v in enumerate( vocab ):
#         o.write( v+" "+vectors[i]+"\n" )
#     o.close()
#
# #             edu = l.split()[0]
# #             vec = l.split()[1:]
# #             if not edu in embeddings:
# #             embeddings.append( edu = vec
# #             else:
# #                 print( "Twice and EDU!", edu )
#
# #     dim = len( list( embeddings.values() )[0] )
# #
# #     o = open( os.path.join( outpath, 'edu_embeddings' ), 'w' )
# #     o.write( str( len( embeddings ) )+" "+str(dim)+"\n"  )
# #     for e in embeddings:
# #         if e == "20646":
# #             print( ">>>>>WTF", f )
# #         o.write( e+" "+" ".join( vec ).strip()+"\n" )
# #     o.close()
#
# def embed( rstdt, outpath, wd_embed, dim_kept=50 ):
#     embed_files = []
#     # mydir has to be the language code (used to find the mebedding file)
#     for mydir in [d for d in os.listdir( rstdt ) if not d.startswith('.') and not d == "eu"]:
#         lguage = mydir[:2]
#         print( "\n-- Reading data for", mydir, lguage  )
#
#         out_file = os.path.join( outpath, lguage+"_edu_embeddings" )
#
#         # - Read wd embeddings file
#         # no vocab file, can t use these files
#         # d = np.load( '/Users/chloe/data/wd_embeddings/embeddings_bible_eurofull/de0.vecs.npy' )
#         # Orginaly 500 dim, keep only 50 for now
#         wd_embed_file = [os.path.join( wd_embed, f ) for f in os.listdir( wd_embed ) if f.startswith( lguage ) and f.endswith( '.vecs')]
#         if len( wd_embed_file ) == 0:
#             print( "No embed file found for: "+mydir )
#             continue
# #             sys.exit( "No embed file found for: "+mydir )
#         wd_embed_file = wd_embed_file[0]
#
#         wd_embed_lines = open( wd_embed_file ).readlines()
#         wd_vect = {l.split()[0]:np.array([float(i) for i in l.split()[1:][:dim_kept]]) for l in wd_embed_lines}
#         dim = list(wd_vect.values())[0].shape[0]
#         print( "Wd embeddings file:", wd_embed_file, "\nWd embeddings dimensions:", dim, "\n\#Words", len(wd_vect) )
#         # Add a vector for unknown
#         addUnkVector( wd_vect, word="*UNK*" )
#
#         # find the tbk files
#         train = os.path.join( rstdt, mydir, "no-translation", "train.tbk" )
#         dev = os.path.join( rstdt, mydir, "no-translation", "dev.tbk" )
#         test = os.path.join( rstdt, mydir, "no-translation", "test.raw" )
#
# #         train = os.path.join( rstdt, mydir, "train.tbk" )
# #         dev = os.path.join( rstdt, mydir, "dev.tbk" )
# #         test = os.path.join( rstdt, mydir, "test.raw" )
#
#         # EDU --> vect
#
#         embeddings = {}
#         if os.path.isfile( train ):
#             print( "\n>Embed TRAIN" )
#             train_edus = read_tbk( train )
#             getEmbed( train_edus, embeddings, wd_vect, unk="*UNK*" )
#         elif os.path.isfile( os.path.join( rstdt, mydir, "train.tbk" ) ):
#             train = os.path.join( rstdt, mydir, "train.tbk" )
#             print( "\n>Embed TRAIN" )
#             train_edus = read_tbk( train )
#             getEmbed( train_edus, embeddings, wd_vect, unk="*UNK*" )
# #
#         print( "\n>Embed DEV" )
#         if os.path.isfile( dev ):
#             dev_edus = read_tbk( dev )
#             getEmbed( dev_edus, embeddings, wd_vect, unk="*UNK*" )
#         elif os.path.isfile( os.path.join( rstdt, mydir, "dev.tbk" ) ):
#             dev = os.path.join( rstdt, mydir, "dev.tbk" )
#             dev_edus = read_tbk( dev )
#             getEmbed( dev_edus, embeddings, wd_vect, unk="*UNK*" )
#         else:
#             sys.exit( "No dev file" )
#
#         print( "\n>Embed TEST" )
#         # For the test, only raw files
#         if os.path.isfile( test ):
#             test_edus = read_raw( test )
#             getEmbed( test_edus, embeddings, wd_vect, unk="*UNK*" )
#         elif os.path.isfile( os.path.join( rstdt, mydir, "no-translation", "testa.raw" ) ):
#             test = os.path.join( rstdt, mydir, "no-translation", "testa.raw" )
#             test_edus = read_raw( test )
#             getEmbed( test_edus, embeddings, wd_vect, unk="*UNK*" )
#         elif os.path.isfile( os.path.join( rstdt, mydir, "test.raw" ) ):
#             test = os.path.join( rstdt, mydir, "test.raw" )
#             test_edus = read_raw( test )
#             getEmbed( test_edus, embeddings, wd_vect, unk="*UNK*" )
#         else:
#             sys.exit( "No test file" )
#
#         vocab_size = len( embeddings )
#         dim_edu_embed = dim_kept*len(FEATS)
#
#
#         o = open( out_file, 'w' )
#         o.write( str( len( embeddings ) )+" "+str( dim_edu_embed )+"\n" )
#         for e in embeddings:
#             o.write( e+" "+" ".join( [str(v) for v in embeddings[e]] )+"\n" )
#         o.close()
#
#         print( "\nEDU embeddings file:", out_file, "\nEDU embeddings dimensions:", dim_edu_embed, "\n\#EDUs", vocab_size )
#
#         embed_files.append( out_file )
#     return embed_files
#
def getEmbed( edus, embeddings, wd_embed, unk="*UNK*" ):
    words = set()
    unk_words = set()

    print( "\# of EDUs", len( edus ) )
    for edu in edus:
        edu_wd = edu['word']
        # Keep the order of FEATS
        wd_repr_edu = [edu[k] for k in FEATS]
#         print( wd_repr_edu )
        if len( wd_repr_edu ) != len( FEATS ):
            sys.exit( "Not all kept fields found for the current EDU" )

#         vect_repr_edu = np.concatenate( [wd_embed[wd.lower()] if wd in wd_embed else wd_embed["*UNK*"] for wd in wd_repr_edu] )
        vect_repr_edu_t = []
        for wd in wd_repr_edu:
            if wd in wd_embed:
                vect_repr_edu_t.append( wd_embed[wd] )
#                 vect_repr_edu = np.concatenate( vect_repr_edu,wd_embed[wd] )
            elif wd.lower() in wd_embed:
                wd = wd.lower()
                vect_repr_edu_t.append( wd_embed[wd] )
#                 vect_repr_edu = np.concatenate( vect_repr_edu,wd_embed[wd.lower()] )
            elif wd == "-LRB-":
                if '(' in wd_embed:
                    wd = '('
                    vect_repr_edu_t.append( wd_embed[wd] )
#                     vect_repr_edu = np.concatenate( vect_repr_edu,wd_embed['('] )
            elif wd == "-RRB-":
                if ')' in wd_embed:
                    wd = ')'
                    vect_repr_edu_t.append( wd_embed[wd] )
#                     vect_repr_edu = np.concatenate( vect_repr_edu,wd_embed[')'] )
            else:
                vect_repr_edu_t.append( wd_embed[unk] )
#             words.add( wd )
#             if not wd in wd_embed:
#                 unk_words.add( wd )

        vect_repr_edu = np.concatenate( [ve for ve in vect_repr_edu_t] )

#         if edu_wd in embeddings:
#             print( "WoW, twice the same EDU", edu_wd )
        embeddings[edu_wd] = vect_repr_edu

        for wd in edu_wd.split('__'):
            words.add( wd )
            if not wd in wd_embed and not wd.lower() in wd_embed:
                unk_words.add( wd )

    print( "\#Words", len( words ), "\#Unk words", len( unk_words ) )
    return unk_words
#     print( unk_words )


def addUnkVector( wd_vect, word="*UNK*" ):
#     print( list(wd_vect.values())[0].shape )
#     print( np.array( list(wd_vect.values()) ) )
    unk_vect = np.average( list( wd_vect.values() ), axis=0 )
    if word in wd_vect:
        sys.exit( "Already a vector for unk words?" )
    wd_vect[word] = unk_vect


def read_raw(filename):
    print( "Reading raw" )
    edus = []
    inFile = open(filename)
    bfr = inFile.readline()#First line = list of categories
    cats = bfr.split()
    bfr = inFile.readline()
    while bfr != '':
        tbfr = bfr.strip()
        if not tbfr == '':
#         if not tbfr.isspace()  and not tbfr == '' and not (tbfr[0] == '<' and tbfr[-1]=='>'):
            var_dic = {}
            fields = tbfr.split()
            for i,c in enumerate(cats):
                var_dic[c] = fields[i]
            edus.append( var_dic )
        bfr = inFile.readline()
    inFile.close()
    return edus


def read_tbk(filename):
    edus = []
    inFile = open(filename)
    bfr = inFile.readline()#First line = list of categories
    cats = bfr.split()
    bfr = inFile.readline()
    while bfr != '':
        tbfr = bfr.strip()
        if not tbfr.isspace()  and not tbfr == '' and not (tbfr[0] == '<' and tbfr[-1]=='>'):
            var_dic = {}
            fields = tbfr.split()
            for i,c in enumerate(cats):
                var_dic[c] = fields[i]
            edus.append( var_dic )
        bfr = inFile.readline()
    inFile.close()
    return edus


if __name__ == '__main__':
    main(  )
