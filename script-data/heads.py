#/usr/bin/env python

"""
A python 3 script used to head annotate aligned ptb ~ CONLL treebanks
"""
import sys
import LabelledTree
import PropagTable #french test to be removed
import fastptbparser as parser
import collections
import os.path




#BASQUE bugfix -> performs a low reattachment of punctuation
def get_offending_punct(tree,offendingdict,idx=0):

    if tree.is_leaf():
        return (idx+1)
    else:
        if len(tree.children) > 0 and tree.children[0].label.startswith('PUNT') and idx > 0:
            offendingdict[idx]= tree.children[0]
        for child in tree.children:
            idx = get_offending_punct(child,offendingdict,idx)
        return (idx)

#Be careful that movements will mess up indexing
def transform_basque_tree(tree,offendingdict,idx=0):

    if tree.is_leaf():
        return (idx+1)
    else:
        if tree.children[0].is_tag() and idx in offendingdict:#remove offending node
            tree.children = tree.children[1:]
            idx += 1 #check this !!
        newchildren = []
        for child in tree.children:
            idx = transform_basque_tree(child,offendingdict,idx)
            newchildren.append(child)
            if child.is_tag() and idx in offendingdict:#adds offending node
                newchildren.append(offendingdict[idx])
        tree.children=newchildren
        return idx

def transformBasque(tree):
    """
    Pushes punctuations to the prev dep in Basque trees (systematic error in the data)
    """
    d = { } #dict of (index,punt tag node) to be moved
    N = len(tree.tree_yield())
    S = tree.do_flat_string()
    get_offending_punct(tree,d)
    transform_basque_tree(tree,d)
    M = len(tree.tree_yield())
    if M != N:
        print('Before',N,S)
        print('After',M,tree.do_flat_string())

def fix_basque_dataset(language_dir):
    """
    Moves incorrect punctuations to their right position
    """
    paths = SpmrlPaths(language_dir)
    sys.stderr.write("Processing %s \n"%language_dir)
    sys.stderr.write("Fixing data...\n")
    for ptb_file in paths.gold_ptb_paths :
        ptb_bank_stream = open(ptb_file,encoding='utf-8')
        ptb_out_stream = open(ptb_file+'.fix','w',encoding='utf-8')
        ptb_str = ptb_bank_stream.readline()
        while ptb_str != '':
            ptb_tree = parser.parse_line(ptb_str)
            transformBasque(ptb_tree)
            ptb_str = ptb_bank_stream.readline()
            print(ptb_tree.do_flat_string(),file=ptb_out_stream)

class HeadLogger:

    def __init__(self,filename):
        self.outstream = open(filename,'w',encoding='utf-8')
        self.logs = []

    def log_error(self,ptb_tree,conll,indexes):
        self.logs.append((ptb_tree,conll,indexes))

    def flush(self):
        s = r"""
        \documentclass[10pt]{article}
        \usepackage[utf8]{inputenc}
        \usepackage{algpseudocode}
        \usepackage{tikz-dependency}
        \usepackage{tikz-qtree}
        \begin{document}
        """
        self.outstream.write(s)
        for(ptb,conll,idxes) in self.logs:
            self.outstream.write('\paragraph{Example}\n')
            self.outstream.write('\Tree '+ptb_as_latex(ptb)+'\n')
            self.outstream.write(conll.as_latex()+'\n')
            self.outstream.write(r"\begin{verbatim}Offending indexes: "+ ",".join([str(idx) for idx in idxes]) + r"\end{verbatim}"+'\n')
        s = r'\end{document}'
        self.outstream.write(s)

    def close(self):
        self.flush()
        self.outstream.close()


#Make it global
LOGGER = HeadLogger('log.tex')

def tree_strip_spmrl(root,tags=True):
    """
    @param unidec : activates transliteration
    @param tags: strip tags
    """
    if root.is_tag():
        if tags:
            root.label = label_strip_spmrl(root.label)
    else:
        root.label = label_strip_spmrl(root.label)
        for child in root.children:
            tree_strip_spmrl(child,tags)


def label_strip_spmrl(label):
    slabel = label.split('##')[0]
    slabel = slabel.split('-')[0]
    return slabel

def ptb_as_latex(root):
    """
    ptb tree as chiang latex
    """
    if root.is_leaf():
        return root.label.replace('_','$\_$')
    else:
        return "[."+' '.join([root.label.replace('_','$\_$')]+[ptb_as_latex(child) for child in root.children]+[' ] '])

def do_headed_spmrl_tree(root,mark='-head',mark_head=True):
    if root.is_leaf(): #fixes a bug in SPRML german data set
        return root.label+mark
    if root.is_tag():
        fvec = []
        cat = root.label
        if '##' in root.label:#another bug fix in german data set
            (cat,feats,_) = root.label.split('##')
            fvec = feats.split('|')
        if mark_head:
           feats = '|'.join(fvec+['head=true'])
        else:
           feats = '|'.join(fvec+['head=false'])
        return '('+'##'.join([cat,feats,''])+" %s)"%(root.children[0])
    elif not root.is_leaf() and mark_head:
        return '(' + ' '.join([root.label+ mark]+[do_headed_spmrl_tree(child,mark=mark,mark_head=child.is_head) for child in root.children ]) + ')'
    elif not root.is_leaf() and not mark_head:
        return '(' + ' '.join([root.label]+[do_headed_spmrl_tree(child,mark=mark,mark_head=child.is_head) for child in root.children]) + ')'

class GrammarRule:

    def __init__(self,symbols,localhead = -1):
        """
        @param symbols: a list with rule symbols
        @param localhead : the index of the head in symbols
        """
        self.symbols = tuple(symbols)
        self.localhead = localhead

    def __str__(self):
        return "%s --> %s [%d]"%(self.symbols[0],' '.join(self.symbols[1:]),self.localhead)

    def __hash__(self):
        return hash(tuple(list(self.symbols)+[self.localhead]))

    def __eq__(self,other):
        return self.symbols == other.symbols and self.localhead == other.localhead

    def hamming_distance(self,other):
        return sum([ x!=y for (x,y) in zip(self.symbols,other.symbols)])

    def arity(self):
        return len(self.symbols)

class ConllXGraph:
    """
    A simple untyped dependency graph where vertices are word indexes and edges couples of word indexes.
    The graph is encoded using a reflexive idom relation.
    """
    def __init__(self):
        self.idom = { }
        self.wordlist = [ ]

    def __str__(self):
        wordstr = ','.join('(%d,%s)'%(idx+1,word) for idx,word in enumerate(self.wordlist))
        edgestr = ','.join(["(%d,%d)"%(key,elt) for key in self.idom for elt in self.idom[key]])
        return "V={%s}\nE={%s}"%(wordstr,edgestr)

    def get_root_idx(self):
        return list(self.idom[0])[0]

    def as_latex(self):
        """
        generates a tikz dependency latex code
        usepackage{tikz-dependency}
        """
        edgestr = '\n'.join(['                                \depedge{%d}{%d}{--}'%(gov,dep) for gov in self.idom for dep in self.idom[gov] if gov !=0])
        wordstr = "\&".join(self.wordlist)
        wordstr = wordstr.replace('_','$\_$')
        root_idx = self.get_root_idx()
        return r"""
                   \scalebox{0.5}{
                   \begin{dependency}[hide label]
                      \begin{deptext}[column sep=.5cm]
                        %s \\
                      \end{deptext}
                      \deproot{%d}{root}
                      %s
                   \end{dependency}
                   }
                """ %(wordstr,root_idx,edgestr)

    def add_word(self,word):
        """
        This adds a string to the input sequence
        """
        self.wordlist.append(word)

    def add_edge(self,gov_idx,dep_idx):
        if gov_idx in self.idom:
            self.idom[gov_idx].add(dep_idx)
        else:
            self.idom[gov_idx] = set([dep_idx])

    def leaf_indexes(self):
        """
        Finds out the set of leaves of this graph
        """
        all_indexes  = set(range(len(self.wordlist)+1))
        return list(all_indexes-set(self.idom.keys()))

    def dom_transitive_closure(self):
        """
        Computes the transitive (non reflexive) closure of dom
        (assumes a tree or a dag).
        """
        self.tdom = {}
        #Base:
        leaves = self.leaf_indexes()
        for leaf_idx in leaves:
            self.tdom[leaf_idx] = set([])
        #Recurrence:
        agenda = list( set(self.idom.keys()) - set(self.tdom.keys()) )

        c = 0
        N = len(agenda)
        while agenda:
            prev_n = len(agenda)
            node_idx = agenda.pop()
            if all( [elt in self.tdom for elt in self.idom[node_idx]]):
                closure = set(self.idom[node_idx])
                for elt in self.idom[node_idx]:
                    closure = closure.union(self.tdom[elt])
                self.tdom[node_idx] = closure
            else:
                agenda.insert(0,node_idx)
            post_n = len(agenda)
            if prev_n == post_n:
                c+=1
            else:
                c = 0
            if c >= N:
                raise CyclicGraphException()

    def dominatesN(self,hyp_gov_idx,siblings):
        """
        Tells how many siblings this node index dominates in this dep tree
        """
        if hyp_gov_idx in self.tdom:
            domset = self.tdom[hyp_gov_idx]
            sset = set(siblings)
            sset.remove(hyp_gov_idx)
            return len(domset.intersection(sset))
        else:
            return 0

    def dominates_em_all(self,hyp_gov_idx,hyp_deps_idxes): #lord of the rings style function :)
        """
        Tells if all hyp_deps are dominated by hyp_gov.
        One can pass all the globalhead indexes of a tree rule rhs to the function.
        """
        if len(hyp_deps_idxes) == 1 :#if only one symbol in RHS trivially head
            return True
        if hyp_gov_idx in self.idom: #if in idom -> potential governor
            deps_set = set(hyp_deps_idxes)
            deps_set.remove(hyp_gov_idx)
            return deps_set.issubset(self.tdom[hyp_gov_idx])
        return False #if not in idom -> not a governor

    @staticmethod
    def get_graph(stream):
        """
        Reads in a graph from stream and returns it
        Returns None when the end of stream has been reached
        """
        g = ConllXGraph()
        bfr = stream.readline()
        while bfr.isspace() or bfr == '':
            if bfr == '':
                return None
            bfr = stream.readline()
        while not bfr.isspace():
            if bfr.isspace() or bfr == '':
                return g
            fields = bfr.split()
            g.add_word(fields[1])
            g.add_edge(int(fields[6]),int(fields[0]))
            bfr = stream.readline()


        return g


###################################################
#
# Main functions
#
###################################################

class CyclicGraphException(BaseException):

    def __init__(self):
        pass
    def __str__(self):
        return '(warning) cyclic conll graph detected.'

class InvalidMatchException(BaseException):

    def __init__(self,index_list,dep_doms):
        self.indexes= index_list
        self.dep_doms = dep_doms

    def __str__(self):
        return 'Offending indexes : {%s},Depgraph dependencies : %s'%(','.join(map(lambda x:str(x),self.indexes)),self.dep_doms)

def get_valid_headrules_fromtree(ptb_root,conll_tree,ruleset,idx=1):

    if not ptb_root.is_tag():
        cidx = idx
        for child in ptb_root.children:
            cidx = get_valid_headrules_fromtree(child,conll_tree,ruleset,cidx)

        ptb_root.hidx = -1
        #print (ptb_root.label,list([elt.label for elt in ptb_root.children]))
        head_idxes = list([child.hidx for child in ptb_root.children])
        conll_tree.dom_transitive_closure()

        argmaxes = []
        nmax = 0
        for hyp_idx in head_idxes:
            n = conll_tree.dominatesN(hyp_idx,head_idxes)
            if n == nmax:
                argmaxes.append(hyp_idx)
            if n > nmax:
                nmax = n
                argmaxes = [hyp_idx]
        if len(argmaxes) == 1:
             ptb_root.hidx = argmaxes[0]

            #if conll_tree.dominates_em_all(hyp_idx,head_idxes):
            #    ptb_root.hidx = hyp_idx
            #    break
        if ptb_root.hidx == -1:
            raise InvalidMatchException(head_idxes,list([(hidx,conll_tree.idom[hidx]) for hidx in head_idxes if hidx in conll_tree.idom]))
        local_idx = -1
        for i,c in enumerate(ptb_root.children):
            if ptb_root.hidx == c.hidx:
                local_idx = i+1
        g = GrammarRule([ptb_root.label]+[child.label for child in ptb_root.children],local_idx)
        ruleset.add(g)
        #print (g)
        return cidx
    else:
        ptb_root.hidx = idx
        return idx+1

def get_knn_dataset(ptb_bank_stream,dep_bank_stream):
    """
    This reads in two aligned spmrl streams and returns the successfully annotated head annoted rules.
    Much like a train function
    """
    rlist = set([ ])

    tot_trees = 0
    succ_trees = 0
    dep_tree = ConllXGraph.get_graph(dep_bank_stream)
    ptb_str = ptb_bank_stream.readline()

    c=0
    while ptb_str != '' and dep_bank_stream != None:
        ptb_tree = parser.parse_line(ptb_str)
        tree_strip_spmrl(ptb_tree)
        try:
            get_valid_headrules_fromtree(ptb_tree,dep_tree,rlist)
            succ_trees += 1
        except InvalidMatchException as e:
            if(len(ptb_tree.tree_yield()) < 15):
                LOGGER.log_error(ptb_tree,dep_tree,e.indexes)
                #print(ptb_as_latex(ptb_tree))
                #print(dep_tree)
                #print(dep_tree.tdom)
                #print(e)
        except CyclicGraphException as e:
            print(e)
        tot_trees += 1
        dep_tree = ConllXGraph.get_graph(dep_bank_stream)
        ptb_str = ptb_bank_stream.readline()
        c+=1

    sys.stderr.write("Processed %d trees out of which %d were successfully processed.\n"%(tot_trees,succ_trees))
    return list(rlist)


def knn(rule,full_rule_list,K):
    """
    Returns the most common local index for the nearest neighbouring rules found in full_rule_list
    """
    restr_list = list([r for r in full_rule_list if r.arity() == rule.arity()])
    if len(restr_list) > 0:
        dist_rules = list(zip([rule.hamming_distance(elt) for elt in restr_list],restr_list))
        dist_rules.sort(key=lambda x: x[0])
        votes = collections.Counter([ r.localhead for(dist,r) in dist_rules[:K]])
        return  votes.most_common()[0][0]
    else:
        dist_rules = list(zip([rule.hamming_distance(elt) for elt in full_rule_list],full_rule_list))
        dist_rules.sort(key=lambda x: x[0])
        votes = collections.Counter([ r.localhead for(dist,r) in dist_rules[:K]])
        return votes.most_common()[0][0]


def knn_match_annotations(ptb_root,dep_tree,full_rule_set,K,idx = 1):
    """
    Annotates a single tree
    """
    if not ptb_root.is_tag():
        cidx = idx
        for child in ptb_root.children:
            cidx = knn_match_annotations(child,dep_tree,full_rule_set,K,cidx)
        ptb_root.hidx = -1
        #Try to match graph
        head_idxes = list([child.hidx for child in ptb_root.children])
        try:
            dep_tree.dom_transitive_closure()
        except CyclicGraphException as e:
            dep_tree.tdom = dep_tree.idom #poor man's repair
            print(e)

        argmaxes = []
        nmax = 0
        for hyp_idx in head_idxes:
            n = dep_tree.dominatesN(hyp_idx,head_idxes)
            if n == nmax:
                argmaxes.append(hyp_idx)
            if n > nmax:
                nmax = n
                argmaxes = [hyp_idx]
        if len(argmaxes) == 1:
             ptb_root.hidx = argmaxes[0]

        #for hyp_idx in head_idxes:
        #    if dep_tree.dominates_em_all(hyp_idx,head_idxes):
        #        ptb_root.hidx = hyp_idx
        #        break

        if ptb_root.hidx == -1:#if failure to find a match with the graph, guess
            g = GrammarRule([label_strip_spmrl(ptb_root.label)]+[label_strip_spmrl(child.label) for child in ptb_root.children])
            local_idx = knn(g,full_rule_set,K)
            if local_idx-1 < len(ptb_root.children):
                ptb_root.hidx = ptb_root.children[local_idx-1].hidx
            else:
                ptb_root.hidx = ptb_root.children[-1].hidx

        #Annotate
        for c in ptb_root.children:
            c.is_head = True if ptb_root.hidx == c.hidx else False
        return cidx
    else:
        ptb_root.hidx = idx
        return idx+1

def french_head_annotate(ptb_bank_stream,ptb_head_stream):
    """
    Head annotation with head propagation rules for comparison with automated method
    """
    ptab = PropagTable.sym4_table()
    ptb_str = ptb_bank_stream.readline()
    while ptb_str != '':
        ptb_tree = parser.parse_line(ptb_str)
        ptb_tree.index_leaves()
        ptb_tree.head_annotate(ptab)
        ptb_tree.label='ROOT'
        ptb_head_stream.write(do_headed_spmrl_tree(ptb_tree,mark='-head',mark_head=True)+'\n')
        ptb_str = ptb_bank_stream.readline()


def head_annotate(ptb_bank_stream, dep_bank_stream, ptb_head_stream, full_rule_set, K=5,german=False):
    """
    This reads in two aligned spmrl streams and head annotates ptb trees as a side effect.
    Much like a pred function.
    """
    dep_tree = ConllXGraph.get_graph(dep_bank_stream)
    ptb_str = ptb_bank_stream.readline()
    C=0
    while ptb_str != '' and dep_bank_stream != None:
        ptb_tree = parser.parse_line(ptb_str)
        tree_strip_spmrl(ptb_tree,tags=False)
        knn_match_annotations(ptb_tree,dep_tree,full_rule_set,K)
        if german:#fixes a bug in the German data set wrt roots
            nroot = LabelledTree.LabelledTree('ROOT')
            nroot.is_head=True
            nroot.add_child(ptb_tree)
            ptb_tree.is_head = True
            ptb_head_stream.write(do_headed_spmrl_tree(nroot,mark='-head',mark_head=True)+'\n')
        else:
            #ptb_tree.label='ROOT' # ! erases the existing root label !
            ptb_head_stream.write(do_headed_spmrl_tree(ptb_tree,mark='-head',mark_head=True)+'\n')
        #print(do_headed_spmrl_tree(ptb_tree,mark='-head',mark_head=True))
        ptb_str = ptb_bank_stream.readline()
        dep_tree = ConllXGraph.get_graph(dep_bank_stream)
        C+=1
    sys.stderr.write("Successfully processed %d trees.\n"%(C,))


###################################################
#
# Top level SPMRL Functions
#
###################################################


class SpmrlPaths:
    """
    Does SPMRL path management for gold data
    """
    def __init__(self,language_dir):

        relative_dirname = os.path.split(language_dir)
        lang_name = relative_dirname[-1].split('_')[0].lower().title()

        self.gold_ptb_paths = [ ]
        for subset in ['train','dev','test']:
            self.gold_ptb_paths.append(os.path.join(language_dir,'gold','ptb',subset,SpmrlPaths.do_treebank_name(subset,lang_name,'gold','ptb')))
        self.gold_head_paths = [ ]
        for subset in ['train','dev','test']:
            self.gold_head_paths.append(os.path.join(language_dir,'gold','ptb',subset,SpmrlPaths.do_treebank_name(subset,lang_name,'gold','ptb','heads')))
        self.gold_conll_paths = [ ]
        for subset in ['train','dev','test']:
            self.gold_conll_paths.append(os.path.join(language_dir,'gold','conll',subset,SpmrlPaths.do_treebank_name(subset,lang_name,'gold','conll')))

    @staticmethod
    def do_treebank_name(subset,lang,scenario,suffix,heads=''):
        if heads !='':
            return '.'.join([subset,lang,scenario,suffix,heads])
        else:
            return '.'.join([subset,lang,scenario,suffix])


def annotate_gold(language_dir,K=5,german=False):
    """
    Performs head annotation for an SPMRL language
    @param K: the KNN K
    """
    paths = SpmrlPaths(language_dir)

    #if 'BASQUE' in language_dir:
    #    fix_basque_dataset(language_dir)
    #    return

    if 'FRENCH' or 'FDTB' in language_dir:
        for(ptb_file,head_file) in zip(paths.gold_ptb_paths,paths.gold_head_paths):
            sys.stderr.write("Processing %s ... (french rules)\n"%(ptb_file,))
            Fp = open(ptb_file,encoding='utf-8')
            Fh = open(head_file,'w',encoding='utf-8')
            french_head_annotate(Fp,Fh)
            Fp.close()
            Fh.close()
        return


    sys.stderr.write("Processing %s \n"%language_dir)
    sys.stderr.write("Training model...\n")
    R = [ ]
    for(ptb_file,conll_file) in zip(paths.gold_ptb_paths,paths.gold_conll_paths):
        sys.stderr.write("Processing %s ...\n"%(ptb_file,))
        Fp = open(ptb_file,encoding='utf-8')
        Fc = open(conll_file,encoding='utf-8')
        R.extend(get_knn_dataset(Fp,Fc))
        Fp.close()
        Fc.close()
    sys.stderr.write("\nTraining done. %d rules acquired.\n\n"%(len(R),))
    sys.stderr.write("Head annotating...\n")
    for(ptb_file,head_file,conll_file) in zip(paths.gold_ptb_paths,paths.gold_head_paths,paths.gold_conll_paths):
        sys.stderr.write("Processing %s ...\n"%(ptb_file,))
        Fp = open(ptb_file,encoding='utf-8')
        Fc = open(conll_file,encoding='utf-8')
        Fh = open(head_file,'w',encoding='utf-8')
        head_annotate(Fp,Fc,Fh,R,K,german)
        Fp.close()
        Fc.close()
        Fh.close()
    sys.stderr.write("done.\n")

# annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/CHINESE_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/FRENCH_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/FDTB")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/ARABIC_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/BASQUE_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/GERMAN_SPMRL",german=True)
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/HEBREW_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/HUNGARIAN_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/KOREAN_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/POLISH_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/SWEDISH_SPMRL")
#annotate_gold("/Users/bcrabbe/Desktop/SPMRL_2014/ENGLISH_SPMRL")

LOGGER.close()

# ptbf = open('/Users/bcrabbe/Desktop/test-heads.mrg')
# conllf = open('/Users/bcrabbe/Desktop/test-heads.conll')
# dep_tree = ConllXGraph.get_graph(conllf)
# ptb_str = ptbf.readline()
# ptb_tree = parser.parse_line(ptb_str)
# tree_strip_spmrl(ptb_tree)
# rules = set([])
# print (dep_tree)
# get_valid_headrules_fromtree(ptb_tree,dep_tree,rules,idx=1)
# print (len(rules))
# ptbf.close()
# conllf.close()
# ptbf = open('/Users/bcrabbe/Desktop/test-heads.mrg')
# connlf = open('/Users/bcrabbe/Desktop/test-heads.conll')
# head_annotate(ptbf,connlf,sys.stdout,5)


#def main():
    #ptbf = open('/home/mcoavoux/Documents/side_projects/multilingual/data/english-rstdt/constituent_format/train.brackets')
    #conllf = open('/home/mcoavoux/Documents/side_projects/multilingual/data/english-rstdt/dependency_format/train.conll')
    #R = get_knn_dataset(ptbf, conllf)
    #ptbf = open('/home/mcoavoux/Documents/side_projects/multilingual/data/english-rstdt/constituent_format/train.brackets')
    #conllf = open('/home/mcoavoux/Documents/side_projects/multilingual/data/english-rstdt/dependency_format/train.conll')
    #head_annotate(ptbf, conllf, sys.stdout, R, 5)
    ##dep_tree = ConllXGraph.get_graph(conllf)
    ##ptb_str = ptbf.readline()
    ##ptb_tree = parser.parse_line(ptb_str)
    ##tree_strip_spmrl(ptb_tree)
    ##rules = set([])
    ##print (dep_tree)

#main()
