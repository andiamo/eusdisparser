# -*- coding: utf-8 -*-
import os
import sys
import subprocess
import tempfile
import fastptbparser as parser
from LabelledTree import *

"""
This is the main script for generating data for testing the parser in a multilingual setting.
The idea is that it creates an expe directory for a given language by copying the SPMRL data set for this language.

WARNING :: this script must be run with python 3 for proper encoding management.

"""
class SpmrlPaths:
    """
    Does SPMRL path management for SPMRL style data structure
    """
    def __init__(self,source_language_dir,target_language_dir):
        """
        @param source: the source SPMRL dataset top level dir (above all languages)
        @param target: the target SPMRL dataset top level dir where to generate the data
        """
        self.srcbase = source_language_dir
        self.tgetbase = target_language_dir
        self.lang = source_language_dir.split('/')[-1].lower().title().split('_')[0]

    def get_source_dir_path(self,scenario,annotation,subset):
        """
        Returns the full dir path on the system for a language a given scenario, an annotation style and a subset.
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        """
        assert(subset in ['train','dev','test'])
        assert(annotation in ['ptb','conll'])
        assert(scenario in ['gold','pred'])
        
        p = os.path.join(self.srcbase,scenario,annotation,subset)
        return p

    def get_source_treebank_path(self,scenario,annotation,subset,heads=True):
        """
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        """
        assert(subset in ['train','dev','test'])
        assert(annotation in ['ptb','conll'])
        assert(scenario in ['gold','pred'])

        tbname = ''
        if annotation == 'ptb' and heads:
            tbname = '.'.join([subset,self.lang,scenario,annotation,'heads'])
        else:
            tbname = '.'.join([subset,self.lang,scenario,annotation])
            
        p = os.path.join(self.srcbase,scenario,annotation,subset,tbname)
        return p

    def get_source_dict_path(self):
        """
        Returns the path to the morphological dictionary associated to this language
        """
        p = os.path.join(self.srcbase,'mdict','morphdict.txt')
        if os.path.isfile(p):
            return p
        else:
            return None
    
    
    def get_target_dir_path(self,scenario,annotation,subset):
        """
        Returns the full dir path on the system for a language a given scenario, an annotation style and a subset.
        if the path does not exist it creates it
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        """
        assert(subset in ['train','dev','test'])
        assert(annotation in ['ptb','conll'])
        assert(scenario in ['gold','pred'])
        p = os.path.join(self.tgetbase,scenario,annotation,subset)
        try:
            os.makedirs( p, 0O777 )
        except:
            sys.stderr.write('(warning) could not generate dir :%s (already existing ?)\n'%(str(p)))
        return p

        
    def get_target_treebank_path(self,scenario,annotation,subset,format):
        """
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        @param format: 'raw' 'mrg' 'conll' 'tbk' 'mwe'
        """
        assert(subset in ['train','dev','test'])
        assert(annotation in ['ptb','conll'])
        assert(scenario in ['gold','pred','silver'])
        assert(format in ['raw','mrg','conll','tbk','mwe'])

        p = os.path.join(self.tgetbase,scenario,annotation,subset,'treebank.'+format)
        try:
            os.makedirs(os.path.join(self.tgetbase,scenario,annotation,subset), 0O777 )
        except:
            #sys.stderr.write('(warning) could not generate file :%s  (already existing ?) \n'%(str(p)))
            pass
        return p

###############################################################    
## headed PTB trees preprocessing (loading)                  ##

def spmrl_node_preprocessor(node):
    """
    Removes functions and sets heads
    """
    fields = node.label.split('-')
    node.label = fields[0]
    if 'head' in fields[1:]:
        node.is_head = True
    else:
        node.is_head = False

def safe_split(att_val):
    """
    This escapes various bugs found in the data set
    """
    if att_val[-1] == '=':
        return (att_val[:-2],'=')
    fsplit = att_val.split('=')
    if len(fsplit) == 2:
        return fsplit
    else:
        return ('_','_')
 
def sprml_tag_preprocessor(node,normalizer):
    """
    Removes functions and sets heads and features
    """
    (cat,feats,_) = node.label.split('##')
    if isinstance(normalizer,EnglishNormalizer):
        if cat[0] == '-' and cat[-1] == '-':
            cat = cat[1:-1]          
    node.label = cat.split('-')[0]
    node.features = dict([ safe_split(f) for f in feats.split('|') if len(f) > 1 ]) #trashes invalid features
    #if isinstance(normalizer,KoreanNormalizer):
    #    node.label = node.features['cpos'] detrimental
    if isinstance(normalizer,EnglishNormalizer):
        node.label = node.label.replace(':','^')
    if isinstance(normalizer,ChineseNormalizer):
        node.label = node.label.replace('#','^')
        node.children[0].label = node.children[0].label.replace('#','^')
    if isinstance(normalizer,FrenchNormalizer):
        if node.label == 'PONCT':
            node.label = 'PONCTW'
            if node.children[0].label in ['.','!','?'] :
                node.label = 'PONCT'
                    
    node.is_head = node.features['head'] == 'true' if 'head' in node.features else False
    node.features = normalizer.normalize_avm(node.features)
    
def spmrl_mwe_preprocessor(root,normalizer,mwe_val='O'):
    """
    Sets the mwe feature (IOB tagging) on the tree tags
    should be called right after spmrl tree preprocessor (assumes node.features is set)
    """
    if root.is_tag():
        root.features['mwe'] = mwe_val
        if mwe_val.startswith('B_'):
            mwe_val = 'I_'+mwe_val[2:]
        return mwe_val
    else:
        if root.label.endswith('+'):
            mwe_val = 'B_' + root.label
        else:
            mwe_val == 'O'
        for child in root.children:
            mwe_val = spmrl_mwe_preprocessor(child,normalizer,mwe_val)
        if root.label.endswith('+'):
            return 'O'
        else:
            return mwe_val

def spmrl_tree_preprocessor(root,normalizer):
    if not root.is_leaf():
        for child in root.children:
            spmrl_tree_preprocessor(child,normalizer)
        if root.is_tag():
            sprml_tag_preprocessor(root,normalizer)
        else :
            spmrl_node_preprocessor(root)
            
def spmrl_treebank_loader(istream, tree_list,normalizer):
    """
    Reads in a treebank file and appends it to the tree_list.
    The function calls an additional clean up function that manages SPMRL additional annotations (features)
    """
    bfr = istream.readline()
    local_list = []
    while bfr != '':
        tree = parser.parse_line(bfr)
        spmrl_tree_preprocessor(tree,normalizer)
        if isinstance(normalizer,FrenchNormalizer):
            spmrl_mwe_preprocessor(tree,normalizer)
        local_list.append(tree)
        bfr = istream.readline()
    tree_list.extend(local_list)

    
def load_sprml_treebank(trainfile,devfile,testfile,normalizer):
    tf = open(trainfile,mode='rt',encoding='utf-8')
    td = open(devfile,mode='rt',encoding  ='utf-8')
    tt = open(testfile,mode='rt',encoding ='utf-8')
    T  = []
    D  = []
    Tst= []
    spmrl_treebank_loader(tf, T,normalizer)
    spmrl_treebank_loader(td, D,normalizer)
    spmrl_treebank_loader(tt, Tst,normalizer)
    tf.close()
    td.close()
    tt.close()
    return (T,D,Tst)
            
class MarmotWrapper:
    """
    Wraps marmot into python and allows to manipulate it as a python object
    """
    def __init__(self):
        pass
    
    def train(self,train_file,dict_file=None,pos=True):
        try:
            train_file.flush()
            sys.stderr.write("Training CRF tagger...")
            if pos:
                subprocess.check_call(
                    ["java","-Xmx8G","-cp","marmot.jar","marmot.morph.cmd.Trainer","-train-file",
                     "form-index=1,tag-index=2,%s"%(str(train_file.name)),
                     "-tag-morph" ,"false","-quadratic-penalty","0.1","-rare-word-max-freq","5",
                    "-model-file","xxx.marmot"])
            else:
                if dict_file == None:
                    subprocess.check_call(
                        ["java","-Xmx8G","-cp","marmot.jar","marmot.morph.cmd.Trainer","-train-file",
                        "form-index=1,tag-index=2,morph-index=3,%s"%(str(train_file.name)),
                        "-tag-morph" ,"true","-quadratic-penalty","0.1","-rare-word-max-freq","5",
                        "-model-file","xxx.marmot"])
                else:
                    print('with dict')
                    subprocess.check_call(
                        ["java","-Xmx8G","-cp","marmot.jar","marmot.morph.cmd.Trainer",
                         "-train-file","form-index=1,tag-index=2,morph-index=3,%s"%(str(train_file.name)),
                        "-tag-morph" ,"true","-quadratic-penalty","0.1","-rare-word-max-freq","5",
                        "-model-file","xxx.marmot",
                        "-type-dict",str(dict_file),
                       ])                      
        except subprocess.CalledProcessError as e:
            print(e)
            exit(1)
        
    def pred(self,in_file):
        """
        @return tag/word yields ready to be plugged into the trees
        """
        in_file.flush()
        out_file = tempfile.NamedTemporaryFile(mode='r',encoding='utf-8')
        try:
            sys.stderr.write("CRF tagger predictions...\n")
            subprocess.check_call(["java" ,"-cp", "marmot.jar","marmot.morph.cmd.Annotator", 
                                   "--model-file", "xxx.marmot",
                                   "--test-file" ,"form-index=0,%s"%(str(in_file.name),) ,
                                   "--pred-file",str(out_file.name)])
        except subprocess.CalledProcessError as e:
            print(e)
            exit(1)
            
        #Fetching results
        all_preds = []
        bfr       = []
        for line in out_file:
            if line.isspace():
               if len(bfr) > 0:
                   all_preds.append(bfr)
                   bfr = []
            else:
                fields = line.split()
                word = LabelledTree(fields[1])
                tag  = LabelledTree(fields[5])
                tag.add_child(word)
                features = fields[7].split('|')
                tag.features = dict([attval.split('=') for attval in features if attval != '_'])
                bfr.append(tag)
                
        out_file.close()
        return all_preds
            
    def clean(self):
        subprocess.check_call(['rm','-f','xxx.marmot'])

class LanguageNormalizer:
    """
    A class that normalizes attribute and values in the SPMRL data sets
    """
    def __init__(self):
        self.norm_attributes = {} # dict of unnormalized -> normalized features
        #any unnormalized attribute absent of this dict is ignored by the conversion
        self.norm_values = {}#maps keys to their norm values. if key is absent, value is left unchanged

    def get_norm_attributes(self):
        return list(set(self.norm_attributes.values()))
        
    def normalize_avm(self,avm):
        """
        This method has the purpose of transforming an avm
        given as arg into a normalized one
        @return the normalized avm
        """
        navm = {}
        for elt in avm:
            if elt in self.norm_attributes:
                navm[self.norm_attributes[elt]] = avm[elt]
        return navm
            
    def normalize_value(self,val):
        if val in self.norm_values:
            return self.norm_values[val]
        else:
            return val

#Lang specifics
class ArabicNormalizer(LanguageNormalizer):
    """
    Normalizes arabic features
    """
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'gender':'gen','number':'num','case':'case','aspect':'aspect','mood':'mood','cat':'cat','subcat':'subcat'}
        self.norm_values = {'sj':'s','m':'masc','f':'fem'}



class BasqueNormalizer(LanguageNormalizer):
    """
    Normalizes basque features
    """
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'NUM':'num','KAS':'case','ASP':'aspect','MDN':'mood','ERL':'erl','DADUDIO':'dadudio','NORK':'nork','NORI':'nori','NOR':'nor'}
        self.norm_values = {'sj':'s','m':'masc','f':'fem'}
        
    def normalize_avm(self,avm):
        avm = LanguageNormalizer.normalize_avm(self,avm)
        if 'KAS' in avm:
            avm['KAS'] = avm['KAS'].split('_')[0]
        return avm
    
class FrenchNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'g':'gen','n':'num','m':'mood','mwe':'mwe'}

class GermanNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'case':'case','mood':'mood','gender':'gender','number':'number'}

class HebrewNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'gen':'gen','num':'num'}
        
class HungarianNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'Num':'num','SubPOS':'SubPOS','Cas':'case','Mood':'mood'}

class KoreanNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'case-type':'case','case-type1':'case','verb-type1':'vtype','verb-type':'vtype'}

class SwedishNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'verbform':'mood','gender':'gen','number':'num','case':'case','perfectform':'perfectform'}

class PolishNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        self.norm_attributes = {'case':'case','gender':'gen','number':'num','aspect':'aspect','post-prepositionality':'post-prepositionality'}

class EnglishNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
class ChineseNormalizer(LanguageNormalizer):
    def __init__(self):
        LanguageNormalizer.__init__(self)
        
class ConllWord:

    def __init__(self,wordform,tag,idx):
        self.idx = idx
        self.wordform = wordform
        self.tag = tag
        self.head =  0
        
    def __str__(self):
        return '\t'.join([str(self.idx),self.wordform,"-",self.tag,self.tag,"_",str(self.head),'_','_','_'])

class ConllGraph:
    
    def __init__(self):
        """
        On the fly conll graph for generating conll silver data sets
        """
        self.deps2head = {} #maps a dependant to its governor
        self.words = {}

    def add_dependency(self,gov,dep):
        self.deps2head[dep] = [gov]

    def add_word(self,word):
        self.words[word.idx] = word

    def __str__(self):
        return '\n'.join([str(self.words[wd]) for wd in sorted(list(self.words.keys()),key = lambda w: int(w))])

    @staticmethod
    def fromTree(tree,g):
        """
        generates a conll graph from a head annotated tree
        """
        if tree.is_tag():
            return tree.clidx
        else:
            for child in tree.children:            
                hidx = ConllGraph.fromTree(child,g)
                if child.is_head:
                    tree.clidx = hidx
            for child in tree.children:
                if not child.is_head:
                    g.add_dependency(tree.clidx,child.clidx)
                    g.words[child.clidx].head = tree.clidx
            return tree.clidx
        
class TreebankTypeDefinition:
    """
    This class allows to select which features are to be generated under which name in the native and marmot-conll format 
    """
    def __init__(self,normalizer):
        self.normalizer = normalizer
        self.src_list = normalizer.get_norm_attributes()
        self.mapping = self.normalizer.norm_attributes
        assert('word' not in self.src_list)
        assert('tag' not in self.src_list)

    def generate_header(self):
        return '\t'.join(['word','tag']+self.src_list)

    def generate_fvalue(self,key,avm):
        if key in avm:
            return avm[key]
        else:
            return 'Na'
        
    def generate_native_token(self,tok_node,mark_head=False,indent=0):
        if tok_node.is_head and mark_head:
            #                               word                        ,tag                      
            return ' '*indent + '\t'.join([tok_node.first_child().label,tok_node.label+'-head']
                                          +[self.generate_fvalue(var,tok_node.features) for var in self.src_list])
        else:
            #                               word                        ,tag                     
            return ' '*indent + '\t'.join([tok_node.first_child().label,tok_node.label]
                                          +[self.generate_fvalue(var,tok_node.features) for var in self.src_list])

    def generate_native_symbol(self,begin,label,indent,head):
            if begin:
                if head:
                    return ' '*indent + '<'+label+'-head>'
                else: 
                    return ' '*indent + '<'+label+'>'
            else:
                return ' '*indent + '</'+label+'>'
            
    def generate_native_tbk(self,root,indent=0):
        """
        @return the tree as a string in native format
        """
        if not root.is_tag():
            return '\n'.join([self.generate_native_symbol(True,root.label,indent,root.is_head)]
                           + [self.generate_native_tbk(child,indent = indent+3) for child in root.children]
                           + [self.generate_native_symbol(False,root.label,indent,root.is_head)])
        else:
            return self.generate_native_token(root,True,indent)
        
    def generate_native_raw(self,root,tags=False):
        """
        @return the tree yield as a string in native format
        """
        if tags:
            y = root.tag_yield()
            return '\n'.join([self.generate_native_token(tok[0],False,0) for tok in y])
        else:
            return '\n'.join(root.do_flat_string().split())

    def make_conll_features(self,node):
       
        lst =  '|'.join([ '='.join([av[0],av[1]]) for av in node.features.items() if av[0] in self.src_list])
        if len(lst) == 0:
            lst = '_'
        return lst
    
    def generate_conll_marmot(self,root):
        """
        @return the tree as a marmot training file (conll style)
        """
        y = root.tag_yield()
        s = '\n'.join(['\t'.join([str(idx+1),tok[1].label,tok[0].label,self.make_conll_features(tok[0])]) for idx,tok in enumerate(y)])
        return s
    
def do_native_gold_file(ostream,ttd,tree_list,format,tags=True):
    """
    Generates a single gold file from tree list.
    @param format: raw or tbk
    @param tags: says if tags are output in raw mode
    """
    assert(format in ['raw','tbk'])
    print(ttd.generate_header(),file=ostream)
    if format == 'tbk':
        for tree in tree_list:
           print(ttd.generate_native_tbk(tree)+'\n',file=ostream)
    if format == 'raw':
        for tree in tree_list:
            print(ttd.generate_native_raw(tree,tags)+'\n',file=ostream)
            #ostream.write(ttd.generate_native_raw(tree,tags)+'\n\n')

def do_conll_silver_file(tree_list,ostream):
    """
    Generates a conll silver file from head annotated trees
    """
    for tree in tree_list:
        ty = tree.tag_yield()
        g = ConllGraph() 
        for idx,elt in enumerate(ty):
            elt[0].clidx = idx+1
            w = ConllWord(elt[1].label,elt[0].label,elt[0].clidx)
            g.add_word(w)
        ConllGraph.fromTree(tree,g)
        print(str(g)+'\n',file=ostream)

        
def do_native_mrg_file(ostream,tree_list):
    """
    Generates a single ptb mrg file from tree list.
    """
    for tree in tree_list:
        ostream.write(tree.do_flat_string()+'\n')

def do_marmot_trainfile(ostream,ttd,tree_list):
    """
    Generates a single marmot train file from tree list.
    """
    for tree in tree_list:
        ostream.write( ttd.generate_conll_marmot(tree)+'\n\n')
              
def do_marmot_raw(ostream,tree_list):
    for tree in tree_list:
        words = tree.tree_yield()
        ostream.write(('\n'.join([w.label for w in words])+"\n\n"))
                
def do_merge_predicted_tags(ref_treebank,predicted_tags):
    
    assert(len(ref_treebank) == len(predicted_tags))

    for (tree,tag_sequence) in zip(ref_treebank,predicted_tags):
        ty = list([tag for (tag,word) in tree.tag_yield()])
        assert(len(ty)==len(tag_sequence))
        for (ref_tag,pred_tag) in zip(ty,tag_sequence):
                ref_tag.label = pred_tag.label
                ref_tag.features = pred_tag.features
                ref_tag.children = pred_tag.children


def do_language_silver_dataset(spaths,ttd,train_trees,dev_trees,test_trees):
    """
    Generates CONLL trees as converted from the head annoted CFG trees
    """
    L = [train_trees,dev_trees,test_trees]
    for idx,sc in enumerate(['train','dev','test']):
        sys.stderr.write('Processing %s file..\n'%(sc))
        conllf = spaths.get_target_treebank_path('silver','conll',sc,'conll')
        cstream = open(conllf,'wt',encoding='utf-8')
        do_conll_silver_file(L[idx],cstream)
        cstream.close()       
                                        
def do_language_gold_dataset(spaths,ttd,train_trees,dev_trees,test_trees):
    """
    Generates the gold files for a given language
    """
    L = [train_trees,dev_trees,test_trees]
    for idx,sc in enumerate(['train','dev','test']):
        sys.stderr.write('Processing %s file..\n'%(sc))
        ptbk = spaths.get_target_treebank_path('gold','ptb',sc,'tbk')
        praw = spaths.get_target_treebank_path('gold','ptb',sc,'raw')
        pmrg = spaths.get_target_treebank_path('gold','ptb',sc,'mrg')
        ftbk = open(ptbk,'wt',encoding='utf-8')
        fraw = open(praw,'wt',encoding='utf-8')
        fmrg = open(pmrg,'wt',encoding='utf-8')
        do_native_gold_file(ftbk,ttd,L[idx],'tbk')
        do_native_gold_file(fraw,ttd,L[idx],'raw')
        do_native_mrg_file(fmrg,L[idx])
        ftbk.close()        
        fraw.close()
        fmrg.close()


def do_marmot_jackknife(spmrl_paths,ttd,marmot,train_trees,dev_trees,test_trees,K=10):
    """
    Performs a K-way jacknifing with Marmot : yields a pred mode
    """
    dataset = train_trees+dev_trees
    newdataset = []
    N = len(dataset)
    fsize = int(N/float(K))
    for k in range(K):
        sys.stderr.write("Fold %d..."%(k+1))
        pred_fold = list(range(k*fsize,(k+1)*fsize))
        train_fold = list(range(0,k*fsize))+list(range((k+1)*fsize,N))
        if k == K-1:#corrects rounding error for last fold
            pred_fold = list(range(k*fsize,N))
            train_fold = list(range(0,k*fsize))
        #make data sets
        tf = tempfile.NamedTemporaryFile(mode='w',encoding='utf-8')
        tp = tempfile.NamedTemporaryFile(mode='w',encoding='utf-8')
        do_marmot_trainfile(tf,ttd,list([dataset[index] for index in train_fold]))
        do_marmot_raw(tp, list([dataset[index] for index in pred_fold]))
        #make inference
        marmot.train(tf,spmrl_paths.get_source_dict_path(),len(ttd.src_list) == 0)
        newdataset.extend(marmot.pred(tp))
        #retrieve results
        tf.close()
        tp.close()
    #final pred on test
    tf = tempfile.NamedTemporaryFile(mode='w',encoding='utf-8')
    tp = tempfile.NamedTemporaryFile(mode='w',encoding='utf-8')
    do_marmot_trainfile(tf,ttd,dataset)
    do_marmot_raw(tp,test_trees)
    #make inference
    marmot.train(tf,spmrl_paths.get_source_dict_path(),len(ttd.src_list) == 0)
    newtest = marmot.pred(tp)
    #merge results
    new_train = newdataset[:len(train_trees)]
    new_dev   = newdataset[len(train_trees):]
    do_merge_predicted_tags(train_trees,new_train)
    do_merge_predicted_tags(dev_trees,new_dev)
    do_merge_predicted_tags(test_trees,newtest)
    tf.close()
    tp.close()

    
def do_language_pred_dataset(spaths,ttd,train_trees,dev_trees,test_trees,K=10):
    """
    Generates the pred files for a given language
    ** Warning it destructively modifies the treebanks passed as params **
    @param K: the jackknife number of iterations
    """
    sys.stderr.write("Performing a %d-fold jackknife for tagging (be patient)...\n"%K)
    marmot = MarmotWrapper()
    do_marmot_jackknife(spaths,ttd,marmot,train_trees,dev_trees,test_trees,K)
    L = [train_trees,dev_trees,test_trees]
    for idx,sc in enumerate(['train','dev','test']):
        sys.stderr.write('Processing %s file..\n'%(sc))
        ptbk = spaths.get_target_treebank_path('pred','ptb',sc,'tbk')
        praw = spaths.get_target_treebank_path('pred','ptb',sc,'raw')
        pmrg = spaths.get_target_treebank_path('pred','ptb',sc,'mrg')
        ftbk = open(ptbk,'wt',encoding='utf-8')
        fraw = open(praw,'wt',encoding='utf-8')
        fmrg = open(pmrg,'wt',encoding="utf-8")
        do_native_gold_file(ftbk,ttd,L[idx],'tbk')
        do_native_gold_file(fraw,ttd,L[idx],'raw')
        do_native_mrg_file(fmrg,L[idx])
        ftbk.close()        
        fraw.close()
        fmrg.close()



#####################
#Language specifics

def generate_language(spmrl_paths,ttd,jack_K = 10):
    #copy scripts
    (train_trees,dev_trees,test_trees) = load_sprml_treebank(spmrl_paths.get_source_treebank_path('gold','ptb','train',heads=True),
                                                             spmrl_paths.get_source_treebank_path('gold','ptb','dev',heads=True),
                                                             spmrl_paths.get_source_treebank_path('gold','ptb','test',heads=True),
                                                             ttd.normalizer)
     
    #train_trees = list([x for x in train_trees if len(x.tree_yield()) < 20])
    #dev_trees = list([x for x in dev_trees if len(x.tree_yield()) < 20])
    #test_trees = list([x for x in test_trees if len(x.tree_yield()) < 20])
    
    do_language_gold_dataset(spmrl_paths,ttd,train_trees,dev_trees,test_trees)
    do_language_silver_dataset(spmrl_paths,ttd,train_trees,dev_trees,test_trees)
    do_language_pred_dataset(spmrl_paths,ttd,train_trees,dev_trees,test_trees,jack_K)

#FDTB
print('Processing FDTB')
fdtb_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/FDTB','/Users/bcrabbe/SPMRL2015/FDTB')
fdtb_ttd = TreebankTypeDefinition(FrenchNormalizer())
generate_language(fdtb_paths,fdtb_ttd)

     
# #SWEDISH
# print('Processing SWEDISH')
# swedish_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/SWEDISH_SPMRL','/Users/bcrabbe/SPMRL2015/SWEDISH')
# swedish_ttd = TreebankTypeDefinition(SwedishNormalizer())
# generate_language(swedish_paths,swedish_ttd)

# #BASQUE
#print('Processing BASQUE')
#basque_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/BASQUE_SPMRL','/Users/bcrabbe/SPMRL2015/BASQUE2')
#basque_ttd = TreebankTypeDefinition(BasqueNormalizer())
#generate_language(basque_paths,basque_ttd)

# #ENGLISH
#print('Processing ENGLISH')
#english_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/ENGLISH_SPMRL','/Users/bcrabbe/SPMRL2015/ENGLISHALL')
#english_ttd = TreebankTypeDefinition(EnglishNormalizer())
#generate_language(english_paths,english_ttd)

# #FRENCH
#print('Processing FRENCH')
#french_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/FRENCH_SPMRL','/Users/bcrabbe/SPMRL2015/FRENCHALL')
#french_ttd = TreebankTypeDefinition(FrenchNormalizer())
#generate_language(french_paths,french_ttd)

# #POLISH
# print('Processing POLISH')
# polish_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/POLISH_SPMRL','/Users/bcrabbe/SPMRL2015/POLISH')
# polish_ttd = TreebankTypeDefinition(PolishNormalizer())
# generate_language(polish_paths,polish_ttd)

# #HEBREW
# print('Processing HEBREW')
# hebrew_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/HEBREW_SPMRL','/Users/bcrabbe/SPMRL2015/HEBREW')
# hebrew_ttd = TreebankTypeDefinition(HebrewNormalizer())
# generate_language(hebrew_paths,hebrew_ttd)

# #HUNGARIAN
# print('Processing HUNGARIAN')
# hungarian_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/HUNGARIAN_SPMRL','/Users/bcrabbe/SPMRL2015/HUNGARIAN')
# hungarian_ttd = TreebankTypeDefinition(HungarianNormalizer())
# generate_language(hungarian_paths,hungarian_ttd)

# #GERMAN
#print('Processing GERMAN')
#german_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/GERMAN_SPMRL','/Users/bcrabbe/SPMRL2015/GERMAN')
#german_ttd = TreebankTypeDefinition(GermanNormalizer())
#generate_language(german_paths,german_ttd)

# #KOREAN
#print('Processing KOREAN')
#korean_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/KOREAN_SPMRL','/Users/bcrabbe/SPMRL2015/KOREAN')
#korean_ttd = TreebankTypeDefinition(KoreanNormalizer())
#generate_language(korean_paths,korean_ttd)

# #ARABIC
#print('Processing ARABIC')
#arabic_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/ARABIC_SPMRL','/Users/bcrabbe/SPMRL2015/ARABIC')
#arabic_ttd = TreebankTypeDefinition(ArabicNormalizer())
#generate_language(arabic_paths,arabic_ttd)

# #CHINESE
#print('Processing CHINESE')
#chinese_paths = SpmrlPaths('/Users/bcrabbe/Desktop/SPMRL_2014/CHINESE_SPMRL','/Users/bcrabbe/SPMRL2015/CHINESE')
#chinese_ttd = TreebankTypeDefinition(ChineseNormalizer())
#generate_language(chinese_paths,chinese_ttd)



