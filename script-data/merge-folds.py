#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, shutil, argparse




def main( ):
    parser = argparse.ArgumentParser(
            description='Merge X files in input to 1 file in output. For nfold: give arg n, the fold no to take into account. File name format = nfold-n.ext')
    parser.add_argument('--inpath',
            dest='inpath',
            action='store',
            help='Input dir containing the files to be merged.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file.')
    parser.add_argument('--nfold',
            dest='nfold',
            action='store',
            help='N for the fold to be ignored.')
    parser.add_argument('--ext',
            dest='ext',
            action='store',
            help='Extension of the file to read.')
    args = parser.parse_args()


    merge( args.inpath, args.outpath, args.nfold, args.ext )

def merge( inpath, outpath, nfold, ext ):
    with open( outpath, 'w' ) as o:
        print( "Writing TRAIN", nfold )
        file_count = 0
        print( "\tUsing files: ", end = '')
        for f in os.listdir( inpath ):
            #print( f )
            if f.endswith( ext ) and not '-'+str(nfold)+'.'+ext in f:
                lines = open( os.path.join( inpath, f ) ).readlines()
                print( f, end = ' ')
                if file_count == 0: #keep the first lines with cats 
                    o.write( ''.join( [l for l in open( os.path.join( inpath, f ) ).readlines()] ) )
                else:
                    o.write( ''.join( [l for l in open( os.path.join( inpath, f ) ).readlines()[1:]] ) )
                #o.write( '\n')
                file_count += 1
    print()
if __name__ == '__main__':
    main()
