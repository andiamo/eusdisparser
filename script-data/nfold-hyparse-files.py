#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Create files to do cross validation with Hyparse discourse parser
- Merge the input files, all in the same order = constituent format/ dependency format/ parse/ / dev test
- Shuffle all the files/documents keeping the same order for all the files
- cut in 10 folds
(- use the generate_discourse_data script) 

# TODO: doc number are not changed in parse files, then we have the same ID for different doc, coming from
dev or test. Changing the ID is not cool, adding a column with dev/test?

'''

import os, sys, shutil, argparse, random
from nltk import Tree



def main( ):
    parser = argparse.ArgumentParser(
            description='Create files to do cross validation with Hyparse discourse parser.')
    parser.add_argument('--const',
            dest='const',
            action='store',
            help='Input bracketed files.')
    parser.add_argument('--dep',
            dest='dep',
            action='store',
            help='Input dep files.')
    parser.add_argument('--parse',
            dest='parse',
            action='store',
            help='Input parse files.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file.')
    parser.add_argument('--nfold',
            dest='nfold',
            action='store',
            default=10,
            help='Number of folds.')
    parser.add_argument('--seed',
            dest='seed',
            action='store',
            default=123456,
            help='Seed for shuffling the data (Def: 123456).')
    args = parser.parse_args()

    SEED=args.seed 

    #outdir = os.path.join( args.outpath, str(args.nfold)+"-fold" )
    outdir = args.outpath
    if not os.path.isdir( outdir ):
        os.mkdir( outdir )

    # (1a) If merge needed
    lconst, ldep, lparse, cat = merge( args.const, args.dep, args.parse, outdir, seed=SEED )
    # (1b) else:
    #lconst, ldep, lparse, cat = read( args.const, args.dep, args.parse, 'dev' )
    # (2) Split:
    split( lconst, ldep, lparse, cat, outdir, nfold=int(args.nfold) )


def split( lconst, ldep, lparse, cat, outdir, nfold=10 ):
    dir_const, dir_dep, dir_parse = make_dirs( outdir, ['nfold_const', 'nfold_dep', 'nfold_parse'] )
    
    # compute number of doc per fold
    ndoc = len( lconst ) # 1 line = 1 tree
    print( "Total doc =", ndoc )
    ndoc_fold = int(round( ndoc / nfold, 1 ))
    print( "Size fold =", ndoc_fold )
    ndoc_rem = ndoc-ndoc_fold*nfold
    # Write folds
    n = 1
    m = 0
    cur_doc = 0
    nf_const, nf_dep, nf_parse = init_nf_files( n, cat, dir_const, dir_dep, dir_parse )
    while cur_doc < len( lconst ):
        if m >= ndoc_fold:
            # write remaining files
            if ndoc_rem != 0:
                write_const_doc( nf_const, lconst, cur_doc+1 )
                write_dep_doc( nf_dep, ldep, cur_doc+1 )
                write_parse_doc( nf_parse, lparse, cur_doc+1 )
                cur_doc += 1
                ndoc_rem -= 1
            n += 1
            m = 0
            nf_const, nf_dep, nf_parse = init_nf_files( n, cat, dir_const, dir_dep, dir_parse )
        # Write const
        write_const_doc( nf_const, lconst, cur_doc )
        # Write dep
        write_dep_doc( nf_dep, ldep, cur_doc )
        # Write parse
        write_parse_doc( nf_parse, lparse, cur_doc )
        m += 1
        cur_doc += 1


def merge( const, dep, parse, outpath, seed=123456 ):
    doc2info = {} # doc -> [ const tree, dep tree, parse info]
    doc_id = 0
    # Parcourir dev files 
    lconst, ldep, lparse, cat = read( const, dep, parse, 'dev' )
    test_const, test_dep,test_parse, _ = read( const, dep, parse, 'test' )

    # Merge the lists
    lconst.extend( test_const )
    ldep.extend( test_dep )
    lparse.extend( test_parse )

    # Shuffle the 3 structures together
    lconst, ldep, lparse = shuffle_lists( lconst, ldep, lparse, seed=seed )

    # Write merged files
    write_merged( lconst, ldep, lparse, cat, outpath, 'merged' )

    #return merged_const, merged_dep, merged_parse
    return lconst, ldep, lparse, cat

def write_merged( lconst, ldep, lparse, cat, outpath, outfile ):
    merged_const = os.path.join( outpath, outfile+".brackets" )
    merged_dep = os.path.join( outpath, outfile+".conll" )
    merged_parse = os.path.join( outpath, outfile+".parse" )
    with open( merged_const, 'w' ) as b:
        b.write( ''.join( lconst ) )
    with open( merged_dep, 'w' ) as c:
        for i,doc in enumerate( ldep ):
            if i != 0:
                c.write( '\n\n' )
            c.write( '\n'.join( doc ).strip() )
    with open( merged_parse, 'w' ) as p:
        p.write( cat.strip() )
        for i,doc in enumerate( lparse ):
            if i != 0:
                p.write( '\n' )
            p.write( '\n'.join( doc ) )

    
def write_const_doc( nf_const, lconst, i ):
    nf_const.write( lconst[i] )

def write_dep_doc( nf_dep, ldep, i ):
    for j,l in enumerate( ldep[i] ):
        if l.strip() != '':
            nf_dep.write(l.strip()+'\n')
    nf_dep.write( '\n' )

def write_parse_doc( nf_parse, lparse, i ):
    for j,l in enumerate( lparse[i] ):
        if l.strip() != '':
            nf_parse.write(l.strip()+'\n')
    nf_parse.write( '\n' )

def init_nf_files( n, cat, dir_const, dir_dep, dir_parse ):
    nf_const = open( os.path.join( dir_const, 'nfold-'+str(n)+'.brackets' ), 'w' )
    nf_dep = open( os.path.join( dir_dep, 'nfold-'+str(n)+'.conll' ), 'w' )
    nf_parse = open( os.path.join( dir_parse, 'nfold-'+str(n)+'.parse' ), 'w' )
    nf_parse.write( cat.strip()+'\n' )
    return nf_const, nf_dep, nf_parse

def read( const, dep, parse, dset ):
    lconst = read_const( open( os.path.join( const, dset+'.brackets' ), 'r' ).readlines() ) 
    ldep = read_dep( open( os.path.join( dep, dset+'.conll' ), 'r' ).readlines() )
    cat, lparse = read_parse( open( os.path.join( parse, dset+'.parse' ), 'r' ).readlines() )
    return lconst, ldep, lparse, cat

def shuffle_lists( lconst, ldep, lparse, seed=123456 ):
    print( "SHUFFLING", seed )
    c = list(zip(lconst, ldep, lparse))
    random.seed(seed)
    random.shuffle(c)
    lconst, ldep, lparse = zip(*c)
    return lconst, ldep, lparse

def read_const( lines ):
    trees = lines
    #for l in lines:
    #    tree = Tree.fromstring( l )
    #    print( tree.leaves() )
    print( "Doc READ:", len( trees ) )
    return trees

def read_dep( lines ):
    deps = []
    tmp = []
    for l in lines:
        if l == '\n':
            deps.append( tmp )
            tmp = []
        tmp.append( l.strip() )
    deps.append( tmp )
    print( "Doc READ:", len( deps ) )
    return deps

def read_parse( lines ):
    parse = []
    cat = lines[0]
    tmp = []
    for l in lines:
        if l == '\n':
            parse.append( tmp )
            tmp = []
        tmp.append( l.strip() )
    parse.append( tmp )
    print( "Doc READ:", len( parse ) )
    return cat, parse

def make_dirs( outdir, subsname ):
    subsdir = []
    for s in subsname:
        dir_ = os.path.join( outdir, s )
        if not os.path.isdir( dir_ ):
            os.mkdir( dir_ )
        subsdir.append( dir_ )
    return subsdir

#    with open( os.path.join( outpath, 'merged.tbk' ), 'w' ) as tbk:
#        tbk.write( ''.join( open( os.path.join( inpath, 'dev.tbk' ), 'r' ).readlines() ) )
#        tbk.write( ''.join( open( os.path.join( inpath, 'test.tbk' ), 'r' ).readlines()[1:] ) ) # do not repeat the CAT line
#    return tbk,_,_

if __name__ == '__main__':
    main()
