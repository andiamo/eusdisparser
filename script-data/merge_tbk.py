#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, shutil, argparse, random


SEED=123456

def main( ):
    parser = argparse.ArgumentParser(
            description='Merge 2 files in input to 1 file in output with shiffling.')
    parser.add_argument('--train1',
            dest='train1',
            action='store',
            help='First training set.')
    parser.add_argument('--train2',
            dest='train2',
            action='store',
            help='Second training set.')
    parser.add_argument('--outpath',
            dest='outpath',
            action='store',
            help='Output file.')
    args = parser.parse_args()


    merge( args.train1, args.train2, args.outpath )

def merge(train1, train2, outpath):
    random.seed = SEED
    docs1, cats1 = read_tbk( open(train1, 'r').readlines() )

    docs2, cats2 = read_tbk( open(train2, 'r').readlines() )

    print( train1, len(docs1), '\n', train2, len(docs2) )
    if cats1 != cats2:
        print( cats1, '\n', cats2 )

    docs = docs1
    docs.extend( docs2 )
    random.shuffle( docs )
    print ( outpath, len(docs) )
    with open( outpath, 'w' ) as of:
        of.write( cats1.strip()+'\n')
        for d in docs:
            for l in d:
                of.write( l+'\n' )

def read_tbk( lines ):
    docs, cur_doc = [], []
    cats = lines[0]
    for l in lines[1:]:
        l = l.replace( '\n', '')
        cur_doc.append( l )
        if l == '</ROOT>':
            # End of document/tree
            docs.append(cur_doc)
            cur_doc = []
    return docs, cats

if __name__ == '__main__':
    main()