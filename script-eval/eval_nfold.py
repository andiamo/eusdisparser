#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Evaluation of the results of a discourse parser using cross-validation
using the score file output by the expe scripts.
'''



from __future__ import print_function
import argparse, os, sys, shutil, codecs
import numpy as np
from nltk import Tree


PARAMS=['h', 'dc', 'it', 'beam', 'lr']

def main( ):
    parser = argparse.ArgumentParser(
            description='Retrieve best params.')
    parser.add_argument('--scores',
            dest='scores',
            action='store',
            help='scores file.')
    parser.add_argument('--run',
            dest='run',
            action='store',
	    default=-1,
            help='run number.')
    parser.add_argument('--verbose',
            dest='verbose',
            action='store_true',
            help='Other prints.')
    args = parser.parse_args()

    get_best( vars(args)['scores'], run=int(vars(args)['run']), verbose=vars(args)['verbose'] )


def get_best( scores, run=-1, verbose=False ):
    if run > 0:
        experiments = read_nfold_run( scores, run )
    else:
        experiments = read_nfold( scores )
    print( '# of experiments:', len( experiments ) )
    best_ex = best_nfold( experiments, sc=-1 )
    print( best_ex.nfold_scores )
    print( "Parameters: "+' '.join( [t+'='+str(best_ex.parameters[t]) for t in sorted( best_ex.parameters.keys() )] ) )

    print( "    On AVG:  "+'\t'.join( [str(s) for s in best_ex.avg_scores] ) )
    # params2mean: parameter --> all scores with that value, to compute min/max/mean score for a spec param
    # all_scores: param1 -> param2 -> .... -> scores
    params2means, all_scores = get_params2scores( experiments, sc=-1 )
    #printLatex( all_scores )
    if verbose:
        print_summary( params2means )
    #exportCSV( all_scores, os.path.join( os.path.dirname(scores), 'scores.csv' ) )


def best_nfold( experiments, sc=-1):
	'''
	Use the average score over the nfold
	sc=-1 : relation score
    sc=-2 : nuclerity score
    sc=-3 : span score
	'''
	best_score, best_ex = -1, None
	for e in experiments:
		if e.avg_scores[sc] > best_score:
			best_ex = e
			best_score = e.avg_scores[sc]
	return best_ex


def read_nfold_run( scores, run_kept ):
    experiments = []
    with open( scores ) as myfile:
        lines = myfile.readlines()
        for l in lines:
            l = l.strip()
            lsplit = l.split('\t')
            if len( lsplit ) == 4:
                setting = [t.split('=') for t in lsplit[0].strip().split("_")]
                params = {k:float(v) for (k,v) in setting if k != 'set'}
                dset = {k:v for (k,v) in setting if k == 'set'}['set']
                nfold = int(dset.split('#')[0])
                run = int(dset.split('#')[1])
                scores = lsplit[1:]
                ex = Expe( float(params['lr']), float(params['dc']),
                        int( params['h'] ), int(params['it']), int(params['beam']), dset=nfold, run=run )
                # dset = test fold
                ex.sc = [float( s ) for s in scores]
                if run == run_kept:
                    experiments.append( ex )
            else:
                print( "(O-O)", l )

    # Now we need a list of all the 'real' expe, ie 1 expe = 1 set of hyper parameters
    # set of parameters --> scores for fold1, scores for fold 2 ...
    id2expe = {}
    allexpe = []
    for i,e in enumerate( experiments ):
    	#if i != j and e.equal( f.parameters ): # different test set but same set of hyper parameter
    	id_expe=e.toString()
    	if id_expe in allexpe:
    		double = False # TODO should be removed at some point ! probably an append vs a write on a file
    		for t in id2expe[id_expe]:
    			if t.dset == e.dset:
    				double = True
    		if not double:
	    		id2expe[id_expe].append( e )
    	else:
    		allexpe.append( id_expe )
    		id2expe[id_expe] = [e]
    # Check
    cok, cmiss = 0,0
    for i in id2expe:
        if len( id2expe[i] ) < 10:
            cmiss += 1
            #print( "Missing expe for", i, len( id2expe[i]))
            #for e in id2expe[i]:
            #    print( e.dset, e.sc )
        else:
            cok += 1
            #print("All expe available for", i)
    print('#ok:', cok, '#miss:', cmiss)
    # Compute the average over the nfold for all expe
    experiments = []
    for i in id2expe:
    	params = id2expe[i][0].parameters
    	ex = Expe( params['lr'], params['dc'], params['h'], params['it'], params['beam'] )
    	ex.nfold_scores = [y for _,y in sorted( [(int(e.dset), e.sc) for e in id2expe[i]], key=lambda x: x[0] ) ]
    	ex.avg_scores = [sum(y) / len(y) for y in zip(*ex.nfold_scores)]
    	experiments.append( ex )

    return experiments


def read_nfold( scores ):
    experiments = []
    with open( scores ) as myfile:
        lines = myfile.readlines()
        for l in lines:
            l = l.strip()
            if 'fold=' in l:
                l = l.replace( 'fold=', '' )
            lsplit = l.split('\t')
            if len( lsplit ) == 4:
                setting = [t.split('=') for t in lsplit[0].strip().split("_")]
                params = {k:float(v) for (k,v) in setting if k != 'set'}
                dset = {k:int(v) for (k,v) in setting if k == 'set'}['set']
                scores = lsplit[1:]
                ex = Expe( float(params['lr']), float(params['dc']),
                        int( params['h'] ), int(params['it']), int(params['beam']), dset=dset )
                # dset = test fold
                ex.sc = [float( s ) for s in scores]
                experiments.append( ex )
            else:
                print( "(O-O)", l )
    # Now we need a list of all the 'real' expe, ie 1 expe = 1 set of hyper parameters
    # set of parameters --> scores for fold1, scores for fold 2 ...
    id2expe = {}
    allexpe = []
    for i,e in enumerate( experiments ):
    	#if i != j and e.equal( f.parameters ): # different test set but same set of hyper parameter
    	id_expe=e.toString()
    	if id_expe in allexpe:
    		double = False # TODO should be removed at some point ! probably an append vs a write on a file
    		for t in id2expe[id_expe]:
    			if t.dset == e.dset:
    				double = True
    		if not double:
	    		id2expe[id_expe].append( e )
    	else:
    		allexpe.append( id_expe )
    		id2expe[id_expe] = [e]
    # Check
    for i in id2expe:
    	if len( id2expe[i] ) < 10:
    		print( "Missing expe for", i, len( id2expe[i]))
    		for e in id2expe[i]:
    			print( e.dset, e.sc )
    # Compute the average over the nfold for all expe
    experiments = []
    for i in id2expe:
    	params = id2expe[i][0].parameters
    	ex = Expe( params['lr'], params['dc'], params['h'], params['it'], params['beam'] )
    	ex.nfold_scores = [y for _,y in sorted( [(int(e.dset), e.sc) for e in id2expe[i]], key=lambda x: x[0] ) ]
    	ex.avg_scores = [sum(y) / len(y) for y in zip(*ex.nfold_scores)]
    	experiments.append( ex )

    return experiments




def find_expe( experiments, parameters ):
    for e in experiments:
        if e.equal( parameters ):
            return e
    return None

class Expe:
    def __init__( self, lr, dc, h, it, beam, dset="unk", run=None, nfold=None ):
        self.sc = [-1,-1,-1] # Temporary score for one fold

        self.nfold_scores = [] # ordered list of scores
        self.avg_scores = [-1,-1,-1]
        self.test_scores = [-1,-1,-1]

        self.lr = lr
        self.dc = dc
        self.h = h
        self.it = it
        self.beam = beam
        self.dset = dset
        self.run = run
        self.nfold = nfold

        self.parameters = { 'h':self.h, 'dc':self.dc, 'it':self.it, 'beam':self.beam, 'lr':self.lr }

    def equal( self, other_parameters ):
        for k in self.parameters:
            if self.parameters[k] != other_parameters[k]:
                return False
        return True

    def toString( self ):
    	#print( self.parameters, '\n', str(self.parameters) )
    	return str(self.parameters)

def printLatex( expe ):
	print( "\\centering\n\\begin{longtable}{ccccc|ccc|ccc}\\multicolumn{5}{c}{PARAMETERS}&\\multicolumn{3}{c}{Avg on 10 fold}&\\multicolumn{3}{c}{TEST}\\\\")
	print( "ite\t&\th\t&\tdc\t&\tbeam\t&\tlr\t&\tspan\t&\tnuc\t&\trel\t&\tspan\t&\tnuc\t&\trel\\\\" )
	line = ""
	for i in sorted( expe.keys() ):
		for h in sorted(expe[i].keys() ):
			for dc in sorted(expe[i][h].keys() ):
				for beam in sorted(expe[i][h][dc].keys() ):
					for lr in sorted(expe[i][h][dc][beam].keys() ):
						line = '\t&\t'.join( [str(i), str(h), str(dc), str(beam), str(lr)] )
						line += '\t&\t'
						line += '\t&\t'.join( [str(round(s,2)) for s in expe[i][h][dc][beam][lr]] )
						line += '\t&\t'
						line += '\t&\t'.join( [str(s) for s in [-1,-1,-1]] )
						line += '\\\\'
						print( line )
	print( "\\end{longtable}" )

	# print( '\t&\t'.join( [str(i) for i in [e.parameters['it'], e.parameters['beam'], e.parameters['lr'], e.parameters['dc'], e.parameters['h']]] )+'\t&\t'+'\t&\t'.join( [str(round(s,2)) for s in e.avg_scores] )+'\t&\t'+'\t&\t'.join( [str(s) for s in e.test_scores] )+'\\\\' )
	# print("\\end{longtable}")

def exportCSV( expe, filename ):

	with open( filename, 'w') as o:
		o.write( '\t'.join( ['ite', 'h', 'dc', 'beam', 'lr', 'span', 'nuc', 'rel'] )+'\n')
		for i in sorted( expe.keys() ):
			for h in sorted(expe[i].keys() ):
				for dc in sorted(expe[i][h].keys() ):
					for beam in sorted(expe[i][h][dc].keys() ):
						for lr in sorted(expe[i][h][dc][beam].keys() ):
							span, nuc, rel = expe[i][h][dc][beam][lr]
							o.write( '\t'.join( [str(v) for v in [i,h,dc,beam,lr,span, nuc, rel]]))
							o.write('\n')

def get_params2scores( experiments, sc=-1 ):
    params2means = {}
    for p in PARAMS:
        params2means[p] = {}
    all_scores = {}
    for e in experiments:
        h, dc, it, beam, lr = e.h, e.dc, e.it, e.beam, e.lr
        if h in params2means['h']:
            params2means['h'][h].append( e.avg_scores[-1] )
        else:
            params2means['h'][h] = [e.avg_scores[-1]]

        if dc in params2means['dc']:
            params2means['dc'][dc].append( e.avg_scores[-1] )
        else:
            params2means['dc'][dc] = [e.avg_scores[-1]]
        if it in params2means['it']:
            params2means['it'][it].append( e.avg_scores[-1] )
        else:
            params2means['it'][it] = [e.avg_scores[-1]]
        if beam in params2means['beam']:
            params2means['beam'][beam].append( e.avg_scores[-1] )
        else:
            params2means['beam'][beam] = [e.avg_scores[-1]]
        if lr in params2means['lr']:
            params2means['lr'][lr].append( e.avg_scores[-1] )
        else:
            params2means['lr'][lr] = [e.avg_scores[-1]]

        # ---
        if e.it in all_scores:
            if e.h in all_scores[e.it]:
                if e.dc in all_scores[e.it][e.h]:
                    if e.beam in all_scores[e.it][e.h][e.dc]:
                        if e.lr in all_scores[e.it][e.h][e.dc][e.beam]:
                            print( "NO" )
                        else:
                            all_scores[e.it][e.h][e.dc][e.beam][e.lr] = e.avg_scores
                    else:
                        all_scores[e.it][e.h][e.dc][e.beam] = {e.lr:e.avg_scores}
                else:
                    all_scores[e.it][e.h][e.dc] = {e.beam:{e.lr:e.avg_scores}}
            else:
                all_scores[e.it][e.h] = {e.dc:{e.beam:{e.lr:e.avg_scores}}}
        else:
            all_scores[e.it] = {e.h:{e.dc:{e.beam:{e.lr:e.avg_scores}}}}
    return params2means, all_scores

def print_summary( params2means ):
    # ---
    for p in params2means:
        print( "\nPARAMETER ", p, 'mean', 'max', 'min' )
        for v in sorted( params2means[p].keys() ):
            print( p, '=', v, np.mean( params2means[p][v] ), max( params2means[p][v] ), min( params2means[p][v] ) )



if __name__ == '__main__':
    main()



