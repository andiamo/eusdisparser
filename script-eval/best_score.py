#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Evaluation of the results of a discourse parser:
    - Input: pred trees and gold trees (both in the brackets/mrg format
    - Output: span, nuclearity and relation scores
'''



from __future__ import print_function
import argparse, os, sys, shutil, codecs
import numpy as np
from nltk import Tree


PARAMS=['h', 'dc', 'it', 'beam', 'lr']

def main( ):
    parser = argparse.ArgumentParser(
            description='Retrieve best scores.')
    parser.add_argument('--scores',
            dest='scores',
            action='store',
            help='scores file.')
    parser.add_argument('--verbose',
            dest='verbose',
            action='store_true',
            help='Other prints.')
    args = parser.parse_args()

    get_best( vars(args)['scores'], verbose=vars(args)['verbose'] )


def get_best( scores, verbose=False ):
    experiments = read( scores )
    print( '# of experiments:', len( experiments ) )

    best_ex = best_on_dev( experiments, sc=-1 )
    print( "Parameters: "+' '.join( [t+'='+str(best_ex.parameters[t]) for t in sorted( best_ex.parameters.keys() )] ) )
    print( "    On DEV:  "+'\t'.join( [str(s) for s in best_ex.dev_score] ) )
    if best_ex.test_score != None and best_ex.test_score[-1] > 0:
        print( "    On TEST: "+'\t'.join( [str(s) for s in best_ex.test_score] ) )
    else:
        print( "    On TESTa: "+'\t'.join( [str(s) for s in best_ex.testa_score] ) )
        print( "    On TESTb: "+'\t'.join( [str(s) for s in best_ex.testb_score] ) )
    if verbose:
        print_summary( experiments, sc=-1 )

def best_on_dev( experiments, sc=-1 ):
    '''
    sc=-1 : relation score
    sc=-2 : nuclerity score
    sc=-3 : span score
    '''
    best_score, best_ex = -1, None
    for e in experiments:
        if e.dev_score[sc] > best_score:
            best_ex = e
            best_score = e.dev_score[sc]
    return best_ex

def print_summary( experiments, sc=-1 ):
    params2means = {}
    for p in PARAMS:
        params2means[p] = {}
    all_scores = {}
    for e in experiments:
        h, dc, it, beam, lr = e.h, e.dc, e.it, e.beam, e.lr
#         print( h, dc, it, beam, lr )
#         print( e.dev_score )
#         print( e.test_score )
        # ---
        #print( params2means['h'] )
        if h in params2means['h']:
            params2means['h'][h].append( e.dev_score[-1] )
        else:
            params2means['h'][h] = [e.dev_score[-1]]

        if dc in params2means['dc']:
            params2means['dc'][dc].append( e.dev_score[-1] )
        else:
            params2means['dc'][dc] = [e.dev_score[-1]]
        if it in params2means['it']:
            params2means['it'][it].append( e.dev_score[-1] )
        else:
            params2means['it'][it] = [e.dev_score[-1]]
        if beam in params2means['beam']:
            params2means['beam'][beam].append( e.dev_score[-1] )
        else:
            params2means['beam'][beam] = [e.dev_score[-1]]
        if lr in params2means['lr']:
            params2means['lr'][lr].append( e.dev_score[-1] )
        else:
            params2means['lr'][lr] = [e.dev_score[-1]]

        # ---
        if e.it in all_scores:
            if e.h in all_scores[e.it]:
                if e.dc in all_scores[e.it][e.h]:
                    if e.beam in all_scores[e.it][e.h][e.dc]:
                        if e.lr in all_scores[e.it][e.h][e.dc][e.beam]:
                            print( "NO" )
                        else:
                            all_scores[e.it][e.h][e.dc][e.beam][e.lr] = e.dev_score
                    else:
                        all_scores[e.it][e.h][e.dc][e.beam] = {e.lr:e.dev_score}
                else:
                    all_scores[e.it][e.h][e.dc] = {e.beam:{e.lr:e.dev_score}}
            else:
                all_scores[e.it][e.h] = {e.dc:{e.beam:{e.lr:e.dev_score}}}
        else:
            all_scores[e.it] = {e.h:{e.dc:{e.beam:{e.lr:e.dev_score}}}}

#     for it in sorted( all_scores.keys() ):
#         print( "IT:", it )
#         for h in sorted( all_scores[it].keys() ):
#             print( "\tH:", h )
#             for dc in sorted( all_scores[it][h].keys() ):
#                 print( "\t\tDC:", dc )
#                 for beam in sorted( all_scores[it][h][dc].keys() ):
#                     print( "\t\t\tBEAM:", beam )
#                     for lr in sorted( all_scores[it][h][dc][beam].keys() ):
#                         print( "\t\t\t\tLR:", lr )
#                         print( "\t\t\t\t\tSCORE:", all_scores[it][h][dc][beam][lr][-1] )


    # ---
    for p in params2means:
        print( "\nPARAMETER ", p, 'mean', 'max', 'min' )
        for v in sorted( params2means[p].keys() ):
            print( p, '=', v, np.mean( params2means[p][v] ), max( params2means[p][v] ), min( params2means[p][v] ) )


def read( scores ):
    experiments = []
    with open( scores ) as myfile:
        lines = myfile.readlines()[1:]
        for l in lines:
            l = l.strip()
            lsplit = l.split('\t')
            if len( lsplit ) == 4:
                setting = [t.split('=') for t in lsplit[0].strip().split("_")]
                params = {k:float(v) for (k,v) in setting if k != 'set'}
                dset = {k:v for (k,v) in setting if k == 'set'}['set']
                scores = lsplit[1:]
                ex = Expe( float(params['lr']), float(params['dc']),
                        int( params['h'] ), int(params['it']), int(params['beam']), dset=dset )
                if dset == 'dev':
                    ex.dev_score = [float( s ) for s in scores]
                elif dset == 'test':
                    ex.test_score = [float( s ) for s in scores]
                elif dset == 'testa':
                    ex.testa_score = [float( s ) for s in scores]
                    ex.test_score = None
                elif dset == 'testb':
                    ex.testb_score = [float( s ) for s in scores]
                    ex.test_score = None
                else:
                    print( "unk set", dset )
                experiments.append( ex )
            else:
                print( "OoO", l )
    new_experiments = []
    for e in experiments:
        if e.dset == "dev":
            # search test score
            for f in experiments:
                if "test" in f.dset and e.equal( f.parameters ):
                    if f.dset == "test":
                        e.test_score = f.test_score
                    elif f.dset == "testa":
                        e.testa_score = f.testa_score
                    elif f.dset == "testb":
                        e.testb_score = f.testb_score
            new_experiments.append( e )
#             if e.test_score[0] == -1:
#                 print( e.lr, e.dc, e.h, e.it,e.beam, e.dset, e.dev_score, e.test_score )
    return new_experiments



def find_expe( experiments, parameters ):
    for e in experiments:
        if e.equal( parameters ):
            return e
    return None

class Expe:
    def __init__( self, lr, dc, h, it, beam, dset="unk" ):
        self.dev_score = [-1,-1,-1]
        self.test_score = [-1,-1,-1]
        self.testa_score = [-1,-1,-1]
        self.testb_score = [-1,-1,-1]
        self.lr = lr
        self.dc = dc
        self.h = h
        self.it = it
        self.beam = beam
        self.dset = dset

        self.parameters = { 'h':self.h, 'dc':self.dc, 'it':self.it, 'beam':self.beam, 'lr':self.lr }

    def equal( self, other_parameters ):
        for k in self.parameters:
            if self.parameters[k] != other_parameters[k]:
                return False
        return True

if __name__ == '__main__':
    main()



