# EusDisParser: A discourse Parser for Basque

Code for the paper:

Mikel Iruskieta and Chloé Braud, *EusDisParser: improving an under-resourced discourse parser with cross-lingual data*, in Proceedings of the Workshop on Discourse Relation Parsing and Treebanking 2019 (NAACL 2019)

The goals are:

- Build a discourse parser for Basque as performant as possible, possibly using data from another language
- Make a quantitative but also qualitative analysis of the results, in order to better understand what's hard in discourse parsing
- In the future: use this parser to pre-annotate data 

The monolingual experiments are performed using a N-fold cross-validation:

- (Dev) data are split into N folds
- N models are trained using the Nth fold as evaluation set, the rest as training set
- The procedure is repeated X times (i.e. runs)
- The scores are avaraged over the runs
- the best set of parameters found is used to retrain a final model using all the dev data, then evaluated on the test set

## Description of the content of this repo:

**data/**

- eu/
	- dev_test/: dev and test set for Basque
		- .brackets: a rewriting of the original files into a bracketed format, see https://bitbucket.org/chloebt/discourse/src/master/preprocess_rst/
		- .parse: data parsed with a UD parser, used to extract features
		- .conll: a format indicating the head of a relation, since the syntactic parser was based on lexicalized trees (to be used by the disc. parser) 
		- .raw: raw text (to be used by the disc. parser)
		- .tbk: format specific to the parser (to be used by the disc. parser)
		
	- 10_fold/: split of the (dev) data into N fold and X runs
	
- [ ] TODO: add data for other languages

**script-bash/** Scripts to run (more easily) experiments using the discourse parser, especially:

- pipeline.sh: code to prepare data for nfold and train the models 

- [ ] TODO: rename to pipeline_nfold.sh
- [ ] TODO: add evaluation average other the X runs
- [ ] TODO: add a similar script to deal with cross lingual expe

**script-data/** Scripts to prepare data

**script-eval/** Scripts for evaluation

- [ ] TODO: add micro-averaging

**dim/** Files used to give the embedding dimension for each type of symbol / feature

**embeddings/** Files with the pre-computed pre-trained embedding for each EDU, based on the embeddings from Facebook and the Basque Team.

**templates/** Template files used by the parser, can be used to modify the information taken into account, i.e. from neighboring EDUs in the stack, parents etc

**expe/** Command lines for (previous) experiments. TODO: remove

## Using the discourse parser

To install and run the parser, take a look at: https://bitbucket.org/chloebt/discourse/src/master/eacl17-discourseParsing/


