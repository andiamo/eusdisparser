 


# OUTPUT/EXPE dir 
expedir=$1
mkdir -p ${expedir}
# MODELS dir
models=${expedir}
# DATA dir
data=$2
# TEMPLATES file
tpls=$3
# SCORE file
scores=$4
# TRAIN file (.tbk, to be able to make expe using another train set than the one in data)
train=$5
# DEV file
dev=$6
# RAW DEV file
rawdev=$7
# REF DEV file
devref=$8
# DIMENSIONS file
dims=$9
# PARSER SOURCE CODE dir
parser=${10}
# SCRIPT SOURCE CODE dir
src=${11}
# EVAL SOURCE CODE dir
eval=${12}
# EMBEDDINGS file
embeddings=${13}

# RAW TEST file (always data from the DATA dir)
#rawtest=${data}/test.raw
rawtest=sandbox/basque-pardi/data/dev_test/test.raw
# REF TEST file
#testref=${data}/test.brackets
testref=sandbox/basque-pardi/data/dev_test/test.brackets

learnrate=${14}
decay=${15}
beam=${16}


### parameters
it=10       # iterations
threads=18

echo ${parser}

# Optimizing learning rate (lr), decrease constant (dc), size hidden layers (h)

for lr in 0.01 0.02 #0.03
do 
for dc in 1e-5 1e-6 1e-7 0
do

for h in 128 # 64 256
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # model name
    model=${models}/model_2H_h${h}_lr${lr}_dc${dc}
    ##mkdir ${model}
    
    # training command line, log printed in model/trainer_log.txt
    ##echo ./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev}
    ##./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt

    # for each model (i.e. the model dumped at each iteration)
    for i in `seq 1 ${it}`
    do 
        # get missing pieces
        ##for f in encoder ttd embed_dims templates
        ##do
        ##    cat ${model}/${f} > ${model}/iteration${i}/${f}
        ##done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in 32 16 8 4 2 1
        do
            # modifies the model/beam file to choose a custom beamsize
            ##echo ${b} > ${model}/iteration${i}/beam
            # parse dev and test
            ./${parser}nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
            ./${parser}nnp -I ${rawtest} -O ${model}/test_it${i}_beam${b} -m ${model}/iteration${i}
            
            # evaluate dev and test output
            python ${eval}eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=dev_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
            #> ${model}/eval_dev_it${i}_beam${b}
            python ${eval}eval_parser.py --preds ${model}/test_it${i}_beam${b} --gold ${testref} --params set=test_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}
            #> ${model}/eval_test_it${i}_beam${b}

        done
    done
    
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



