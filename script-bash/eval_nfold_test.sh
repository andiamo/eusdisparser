# -----------------------------------------------------------------                  
# EVALUATION                                                                         
                                                                                      
# Retrieve the best set of hyper parameters                                          
# Train on the entire (dev) set                                                      
# Evaluate on test data 

now=$(date +"%m_%d_%Y")

# Directory containing the code and data for the project
src=projects/basque-pardi/ #$1

# Directory containing the original data 
data=${src}/data/dev_test/
# Temp data dir
#data=/home/chloe/sandbox/basque-pardi/data/ 

# Source for the code of the discourse parser
#TODO: stil in a different directory!
src_parser=projects/multilingual/hyparse_fork/build/ #$2
# Source for the bash scripts
src_bash=${src}/script-bash/
# Source code for evaluation of the discourse parser
src_eval=${src}/script-eval/ 
# Output directory for models and prediction files
expe_out=$1
#expe_out=/media/chloe/miya/experiments/basque_2018_nfold-test_${now}/ #$3
mkdir -p ${expe_out}
scores_file=${expe_out}/scores_expe_nfold-test_${now}.txt
echo ${scores_file}
log_file=${expe_out}/log_file_nfold-test_${now}.txt


# TEMPLATES file
tpls=${src}/templates/discourse_ttd_wd_s0s1q0_lr.tpl
# DIMENSIONS file
dims=${src}/dim/dimensions_w350_embed 
# EMBEDDINGS file
embeddings=$2
#embeddings=${src}/embeddings/monolingual_embeddings_basque/fromFacebookWeb/EDU-embed_cc.eu.300.vec.edu
## embeddings=${src}/embeddings/monolingual_embeddings_basque/fromBasqueTeam/EDU-embed_ElhuyarAndWikiModel_cbow_300_chrNGRAM5_win_5_neg_10.vec.edu

# Max number of ITERATIONS
ITERATIONS=10
 
LEARNRATE=${3}
DECAY=${4}
BEAM=${5}
HLAY=${6}


train=${data}/dev.tbk
test=${data}/test.tbk
# RAW TEST file (always data from the DATA dir)
rawtest=${data}/test.raw
# REF TEST file
testref=${data}/test.brackets





threads=18

echo ${src_parser}


for lr in ${LEARNRATE}
do
    for dc in ${DECAY}
    do

	for h in ${HLAY}
	do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # model name
    model=${expe_out}/model_2H_h${h}_lr${lr}_dc${dc}/
    mkdir -p ${model}
    
    # training command line, log printed in model/trainer_log.txt
    echo ./${src_parser}nnt -i ${ITERATIONS} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${test}
    ./${src_parser}nnt -i ${ITERATIONS} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${test} 2> ${model}/trainer_log.txt

    # for each model (i.e. the model dumped at each iteration)
    for i in `seq 1 ${ITERATIONS}`
    do 
        # get missing pieces
        for f in encoder ttd embed_dims templates
        do
            cat ${model}/${f} > ${model}/iteration${i}/${f}
        done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in ${BEAM} #32 16 8 4 2 1
        do
            # modifies the model/beam file to choose a custom beamsize
            echo ${b} > ${model}/iteration${i}/beam
            # parse test
            ./${src_parser}nnp -I ${rawtest} -O ${model}/test_it${i}_beam${b} -m ${model}/iteration${i}
            
            # evaluate test output
            python ${src_eval}eval_parser.py --preds ${model}/test_it${i}_beam${b} --gold ${testref} --params set=test_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores_file}
            #> ${model}/eval_test_it${i}_beam${b}

        done
    done
    
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



