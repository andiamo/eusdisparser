

# OUTPUT/EXPE dir 
expe_out=$1
mkdir -p ${expe_out}

# SCORE file
scores=$2

# DATA dir (TODO: everything in the same dir!)
data=$3
data_tbk_raw=${data}/tbk-raw/
data_brackets=${data}/nfold_const

# PARSER SOURCE CODE dir
src_parser=$4
# EVAL SOURCE CODE dir
src_eval=$5

# EMBEDDINGS file
embeddings=$6
# TEMPLATES file
tpls=$7
# DIMENSIONS file
dims=$8
# Max iterations
it=$9
#Number of FOLDS
N=${10}
#Run id
run=${11}

threads=1


echo ${parser}

# Optimizing learning rate (lr), decrease constant (dc), size hidden layers (h)
for lr in 0.01 0.02 #0.03
do 
for dc in 1e-5 1e-6 1e-7 0
do
for h in 128 64 256
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # for a given set of parameters: train n models
    modeldir=${expe_out}/model_2H_h${h}_lr${lr}_dc${dc}/
    mkdir -p ${modeldir}

    for n in $(seq 1 $N)
    do
        # model name (tested on nfold n)
        model=${modeldir}/model_2H_h${h}_lr${lr}_dc${dc}_n{$n}
        mkdir -p ${model}
    
        # training command line, log printed in model/trainer_log.txt
        # train = merging all the folds except n ; dev = evaluate on fold n
        train=${data}/train_${n}.tbk
        dev=${data_tbk_raw}/nfold-${n}.tbk
        devref=${data_brackets}/nfold-${n}.brackets
        rawdev=${data_tbk_raw}/nfold-${n}.raw

        echo NFOLD ${n} ./${src_parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev}
        ./${src_parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt

        echo
        echo 'TRAIN:' $train 
        echo 'DEV:' $dev $devref $rawdev
        echo

        # for each model (i.e. the model dumped at each iteration)
        for i in `seq 1 ${it}`
        do 
            # get missing pieces
            for f in encoder ttd embed_dims templates
            do
                cat ${model}/${f} > ${model}/iteration${i}/${f}
            done
        
            # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
            for b in 32 16 8 4 2 1
            do
                # modifies the model/beam file to choose a custom beamsize
                echo ${b} > ${model}/iteration${i}/beam
                # parse dev 
                ./${src_parser}/nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
            
                # evaluate fold n output
                python ${src_eval}eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=${n}#${run}_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}

            done
         done
      done 
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



