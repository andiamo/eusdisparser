

# OUTPUT/EXPE dir 
expedir=$1
mkdir -p ${expedir}
# MODELS dir
models=${expedir}
# DATA dir
data=$2
# TEMPLATES file
tpls=$3
# SCORE file
scores=$4

# DIR with NFOLD FILES
nfold=$5

# DIMENSIONS file
dims=$6
# PARSER SOURCE CODE dir
parser=${7}
# SCRIPT SOURCE CODE dir
src=${8}
# EVAL SOURCE CODE dir
eval=${9}
# EMBEDDINGS file
embeddings=${10}


# DATA sOURCE CODE dir
srcd=projects/basque-pardi/script-data

### parameters
it=10       # iterations
threads=1

tmpdata=${data}tmp/
mkdir -p ${tmpdata}

echo ${parser}

#Write the data
#for n in 1 2 3 4 5 6 7 8 9 10
#    do
    	# train = merging all the folds except n
        # dev = evaluate on fold n
#        train=${tmpdata}train_${n}.tbk
#        python ${srcd}/merge-folds.py --inpath ${nfold} --nfold n --outpath ${train} --ext tbk
#
#    done

# Optimizing learning rate (lr), decrease constant (dc), size hidden layers (h)
for lr in 0.01 0.02 #0.03
do 
for dc in 1e-5 1e-6 1e-7 0
do
for h in 128 # 64 256
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # for a given set of parameters: train n models
    modeldir=${models}/model_2H_h${h}_lr${lr}_dc${dc}/
    mkdir -p ${modeldir}

    for n in 1 2 3 4 5 6 7 8 9 10
    do

        # model name (tested on nfold n)
        model=${modeldir}/model_2H_h${h}_lr${lr}_dc${dc}_n{$n}
        mkdir -p ${model}
    
        # training command line, log printed in model/trainer_log.txt
        # train = merging all the folds except n
        # dev = evaluate on fold n
        train=${tmpdata}train_${n}.tbk
        #python ${srcd}/merge-folds.py --inpath ${nfold} --nfold n --outpath ${train} --ext tbk
        dev=${nfold}nfold-${n}.tbk
        devref=${nfold}nfold-${n}.brackets
        rawdev=${nfold}nfold-${n}.raw

        echo NFOLD ${n} ./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev}
        ./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt

        echo
        echo $train $dev $devref $rawdev
        echo

        # for each model (i.e. the model dumped at each iteration)
        for i in `seq 1 ${it}`
        do 
            # get missing pieces
            for f in encoder ttd embed_dims templates
            do
                cat ${model}/${f} > ${model}/iteration${i}/${f}
            done
        
            # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
            for b in 32 16 8 4 2 1
            do
                # modifies the model/beam file to choose a custom beamsize
                echo ${b} > ${model}/iteration${i}/beam
                # parse dev and test
                ./${parser}nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
                
            
                # evaluate fold n output
                python ${eval}eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=${n}_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}

            done
         done
      done 
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



