

# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# Monolingual expe: training on original lgue not translated / Cross validation
# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------


# DATA name = de-pcc ; en-rstdt ; pt-all ; sp-rst
data=$1
# DATA dir
#data=projects/multilingual/new_data/$1/no-translation/
# OUTPUT/EXPE dir
experiments=expe-crossling-ParDi_2018/
mkdir -p ${experiments}
experiments=expe-crossling-ParDi_2018/monoling-noembed-10foldOnDev/ # MONOLINGUE NO EMBEDDINGS CROSSVAL
mkdir -p ${experiments}
expe=${experiments}/
#expe=${experiments}/$1/
#mkdir -p ${expe}
# SYSTEM name
system=monolingual-origin_s0s1q0_lr
# OUTPUT/EXPE SYSTEM dir
expedir=${expe}/${system}
mkdir -p ${expedir}
# PARSER SOURCE CODE dir
parser=projects/multilingual/hyparse_fork/build/
# EVAL SOURCE CODE dir
eval=projects/basque-pardi/script-eval/
# SCRIPT sOURCE CODE dir
src=projects/basque-pardi/script-bash/
# DATA sOURCE CODE dir
srcd=projects/basque-pardi/script-data/
# TEMPLATE file
templates=projects/basque-pardi/templates/discourse_ttd_s0s1q0_lr.tpl
# DIMENSION file
dims=projects/basque-pardi/dim/dimensions_w50


# -- Cross-val
# 1- simple cross val, no opt
# Run X times the CV to optimize the hyper param
# Dir with 10 fold tbk and raw
tbkraw=${data}tbk-raw/
tmpdata=${data}tmp/
mkdir -p ${tmpdata}

for n in 1 2 3 4 5 6 7 8 9 10
    do
    echo ${tbkraw}nfold-$n.tbk
    # TRAIN file = merge all files except n
    train=${tmpdata}/train.tbk
    python ${srcd}/merge-folds.py ${tbkraw} n ${train}
    # no dev file ?

    done

# TRAIN file
train=${data}/train.tbk
# DEV file
dev=${data}/dev.tbk
# RAW DEV file
rawdev=${data}/dev.raw
# REF DEV file
devref=${data}/dev.brackets

# SCORE file
scores=${expedir}/scores_2H.txt
# BEST SCORE file
bsc=${expedir}/best_scores_2H.txt

echo
echo ${system} -- ${data} -- 
echo
echo  >${scores}
echo  >${bsc}

# Union (train+)dev+test



bash ${src}expe.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${rawdev} ${devref} ${dims} ${parser} ${src} ${eval}


# Compute best scores
python ${eval}best_score.py --scores ${scores} >${bsc} 
# Print best scores
tail -n 3 ${bsc}
echo







