

# Simple models: only s0s1q0 + wd features to use embeddings
# templates=discoursecph/multilingual/expe/discourse_ttd_wd_s0s1q0
# embeddings=data/embeddings_edu_rst/edu_embeddings

experiments=experiments/eacl16_multilingualRST/
mkdir -p ${experiments}



# Ue original data, no translation, use multilingual embeddings

#English
endata=discoursecph/multilingual/new_data/en-rstdt/
#German
dedata=discoursecph/multilingual/new_data/de-pcc/no-translation
#Portuguese
ptdata=discoursecph/multilingual/new_data/pt-all/no-translation
#Spanish
spdata=discoursecph/multilingual/new_data/sp-rst/no-translation
#Dutch
nldata=discoursecph/multilingual/new_data/nl-rst/no-translation
#Basque
eudata=discoursecph/multilingual/new_data/eu-rst/no-translation

enexpe=${experiments}/en-rstdt
mkdir -p ${enexpe}
deexpe=${experiments}/de-pcc
mkdir -p ${deexpe}
ptexpe=${experiments}/pt-all
mkdir -p ${ptexpe}
spexpe=${experiments}/sp-rst
mkdir -p ${spexpe}
nlexpe=${experiments}/nl-rst
mkdir -p ${nlexpe}
euexpe=${experiments}/eu-rst
mkdir -p ${euexpe}





# # ----------------------------------------------------------------------------------
# # ----------------------------------------------------------------------------------
# # Mix all corpora to make a new training set + embeddings (SOURCE only)
# # ----------------------------------------------------------------------------------
# # ----------------------------------------------------------------------------------
# # 
# # templates=discoursecph/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
# # dims=discoursecph/multilingual/expe/dimensions_w1400_embed
# # 
# # system=plurilingual-sourceonly-train-eduembed_s0s1q0_lr_w1400
# # echo ${system}
# # 
# # embeddings=data/edu_embed_200x7.txt
# 
# 
# 
# templates=discoursecph/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
# dims=discoursecph/multilingual/expe/dimensions_w350_embed
# 
# system=plurilingual-sourceonly-train-eduembed_s0s1q0_lr_w350_v2
# echo ${system}
# 
# embeddings=data/edu_embed_50x7.txt
# 
# 
# 
# 
# 
# # - GERMAN
# 
# data=${dedata}
# expe=${deexpe}
# echo -- GERMAN --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# # Source only: train on all data except German, optimize also on source only
# train=${expedir}/train_all-de.tbk
# dev=${expedir}/dev_all-de.tbk
# devraw=${expedir}/dev_all-de.raw
# devref=${expedir}/dev_all-de.brackets
# 
# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python discoursecph/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${ptdata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - BRAZILIAN ALL
# data=${ptdata}
# expe=${ptexpe}
# echo -- PORTUGUESE ALL --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-pt.tbk
# dev=${expedir}/dev_all-pt.tbk
# devraw=${expedir}/dev_all-pt.raw
# devref=${expedir}/dev_all-pt.brackets
# 
# 
# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${dedata}/train.tbk --outpath ${train}
# python discoursecph/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${dedata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - SPANISH
# data=${spdata}
# expe=${spexpe}
# echo -- SPANISH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-sp.tbk
# dev=${expedir}/dev_all-sp.tbk
# devraw=${expedir}/dev_all-sp.raw
# devref=${expedir}/dev_all-sp.brackets
# 
# 
# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${ptdata}/train.tbk ${dedata}/train.tbk --outpath ${train}
# python discoursecph/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${ptdata} ${dedata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_spanish_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# 
# 
# # - DUTCH
# data=${nldata}
# expe=${nlexpe}
# echo -- DUTCH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# 
# train=${expedir}/train_all-nl.tbk
# dev=${expedir}/dev_all-nl.tbk
# devraw=${expedir}/dev_all-nl.raw
# devref=${expedir}/dev_all-nl.brackets
# 
# 
# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${dedata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python discoursecph/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${dedata} ${ptdata} --outpath ${dev} ${devraw} ${devref}
# 
# bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - ENGLISH
# data=${endata}
# expe=${enexpe}
# 
# echo -- ENGLISH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-en.tbk
# dev=${expedir}/dev_all-en.tbk
# devraw=${expedir}/dev_all-en.raw
# devref=${expedir}/dev_all-en.brackets
# 
# 
# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${spdata}/train.tbk ${dedata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python discoursecph/multilingual/expe/scripts/union_dev.py --inpath ${spdata} ${dedata} ${ptdata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 
# 
# tail -n 3 ${bsc}
# echo
# 











# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# Mix all corpora to make a new training set + embeddings (SOURCE + TARGET)
# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# 
# templates=discoursecph/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
# dims=discoursecph/multilingual/expe/dimensions_w1400_embed
# 
# system=plurilingual-source+target-train-eduembed_s0s1q0_lr_w1400
# echo ${system}
# 
# embeddings=data/edu_embed_200x7.txt





templates=discoursecph/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
dims=discoursecph/multilingual/expe/dimensions_w350_embed

system=plurilingual-source+target-train-eduembed_s0s1q0_lr_w350_v2
echo ${system}

embeddings=data/edu_embed_50x7.txt



train=${experiments}/train_all-v2.tbk

# python discoursecph/multilingual/expe/scripts/union_train.py --inpath ${dedata}/train.tbk ${endata}/train.tbk ${spdata}/train.tbk ${ptdata}/train.tbk --outpath ${train}




# - GERMAN
data=${dedata}
expe=${deexpe}
echo -- GERMAN --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}


# Optimize on the dev data for the language
bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 

tail -n 3 ${bsc}
echo




# - BRAZILIAN ALL
data=${ptdata}
expe=${ptexpe}
echo -- PORTUGUESE ALL --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo




# - SPANISH
data=${spdata}
expe=${spexpe}
echo -- SPANISH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_spanish_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}


python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo






# - DUTCH
data=${nldata}
expe=${nlexpe}
echo -- DUTCH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}


# Optimize on the dev data for the language
bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}


python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo




# - ENGLISH
data=${endata}
expe=${enexpe}

echo -- ENGLISH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash discoursecph/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python discoursecph/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 

tail -n 3 ${bsc}
echo


