

# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# Monolingual expe: training on original lgue not translated
# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------


# DATA name = de-pcc ; en-rstdt ; pt-all ; sp-rst
data=$1
# DATA dir
data=projects/multilingual/new_data/$1/no-translation/
# OUTPUT/EXPE dir
experiments=expe-crossling-ParDi_2018/
mkdir -p ${experiments}
experiments=expe-crossling-ParDi_2018/monoling-noembed/ # MONOLINGUE NO EMBEDDINGS
mkdir -p ${experiments}
expe=${experiments}/$1/
mkdir -p ${expe}
# SYSTEM name
system=monolingual-origin_s0s1q0_lr
# OUTPUT/EXPE SYSTEM dir
expedir=${expe}/${system}
mkdir -p ${expedir}
# PARSER SOURCE CODE dir
parser=projects/multilingual/hyparse_fork/build/
# EVAL SOURCE CODE dir
eval=projects/basque-pardi/script-eval/
# SCRIPT sOURCE CODE dir
src=projects/basque-pardi/script-bash/
# TEMPLATE file
templates=projects/basque-pardi/templates/discourse_ttd_s0s1q0_lr.tpl
# DIMENSION file
dims=projects/basque-pardi/dim/dimensions_w50
# TRAIN file
train=${data}/train.tbk
# DEV file
dev=${data}/dev.tbk
# RAW DEV file
rawdev=${data}/dev.raw
# REF DEV file
devref=${data}/dev.brackets
# SCORE file
scores=${expedir}/scores_2H.txt
# BEST SCORE file
bsc=${expedir}/best_scores_2H.txt

echo
echo ${system} -- ${data} -- 
echo
echo  >${scores}
echo  >${bsc}

# Specific script for spanish (2 test sets)
if [ $1 = "sp-rst" ]
then
	bash ${src}expe_spanish.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${rawdev} ${devref} ${dims} ${parser} ${src} ${eval}
else
	bash ${src}expe.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${rawdev} ${devref} ${dims} ${parser} ${src} ${eval}
fi

# Compute best scores
python ${eval}best_score.py --scores ${scores} >${bsc} 
# Print best scores
tail -n 3 ${bsc}
echo






