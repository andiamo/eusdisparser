

# # ----------------------------------------------------------------------------------
# # ----------------------------------------------------------------------------------
# # Mix all corpora to make a new training set + embeddings (SOURCE only)
# # ----------------------------------------------------------------------------------
# # ----------------------------------------------------------------------------------

experiments=expe_2018/
mkdir -p ${experiments}
experiments=expe_2018/crossling-embed-source/
mkdir -p ${experiments}


# Use original data, no translation, use multilingual embeddings
#English
endata=projects/multilingual/new_data/en-rstdt/
#German
dedata=projects/multilingual/new_data/de-pcc/no-translation
#Portuguese
ptdata=projects/multilingual/new_data/pt-all/no-translation
#Spanish
spdata=projects/multilingual/new_data/sp-rst/no-translation
#Dutch
nldata=projects/multilingual/new_data/nl-rst/no-translation
#Basque
eudata=projects/multilingual/new_data/eu-rst/no-translation

enexpe=${experiments}/en-rstdt
mkdir -p ${enexpe}
deexpe=${experiments}/de-pcc
mkdir -p ${deexpe}
ptexpe=${experiments}/pt-all
mkdir -p ${ptexpe}
spexpe=${experiments}/sp-rst
mkdir -p ${spexpe}
nlexpe=${experiments}/nl-rst
mkdir -p ${nlexpe}
euexpe=${experiments}/eu-rst
mkdir -p ${euexpe}






templates=projects/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
dims=projects/multilingual/expe/dimensions_w1400_embed
# # 
# # system=plurilingual-sourceonly-train-eduembed_s0s1q0_lr_w1400
# # echo ${system}
# # 
# # embeddings=data/edu_embed_200x7.txt
# 
# 
# 
# templates=projects/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
# dims=projects/multilingual/expe/dimensions_w350_embed
# 
# system=plurilingual-sourceonly-train-eduembed_s0s1q0_lr_w350_v2
# echo ${system}
# 
# embeddings=data/edu_embed_50x7.txt
# 
# 
# 
# 
# 
# # - GERMAN
# 
# data=${dedata}
# expe=${deexpe}
# echo -- GERMAN --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# # Source only: train on all data except German, optimize also on source only
# train=${expedir}/train_all-de.tbk
# dev=${expedir}/dev_all-de.tbk
# devraw=${expedir}/dev_all-de.raw
# devref=${expedir}/dev_all-de.brackets
# 
# python projects/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python projects/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${ptdata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - BRAZILIAN ALL
# data=${ptdata}
# expe=${ptexpe}
# echo -- PORTUGUESE ALL --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-pt.tbk
# dev=${expedir}/dev_all-pt.tbk
# devraw=${expedir}/dev_all-pt.raw
# devref=${expedir}/dev_all-pt.brackets
# 
# 
# python projects/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${dedata}/train.tbk --outpath ${train}
# python projects/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${dedata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - SPANISH
# data=${spdata}
# expe=${spexpe}
# echo -- SPANISH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-sp.tbk
# dev=${expedir}/dev_all-sp.tbk
# devraw=${expedir}/dev_all-sp.raw
# devref=${expedir}/dev_all-sp.brackets
# 
# 
# python projects/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${ptdata}/train.tbk ${dedata}/train.tbk --outpath ${train}
# python projects/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${ptdata} ${dedata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_spanish_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# 
# 
# # - DUTCH
# data=${nldata}
# expe=${nlexpe}
# echo -- DUTCH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# 
# train=${expedir}/train_all-nl.tbk
# dev=${expedir}/dev_all-nl.tbk
# devraw=${expedir}/dev_all-nl.raw
# devref=${expedir}/dev_all-nl.brackets
# 
# 
# python projects/multilingual/expe/scripts/union_train.py --inpath ${endata}/train.tbk ${spdata}/train.tbk ${dedata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python projects/multilingual/expe/scripts/union_dev.py --inpath ${endata} ${spdata} ${dedata} ${ptdata} --outpath ${dev} ${devraw} ${devref}
# 
# bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}
# 
# tail -n 3 ${bsc}
# echo
# 
# 
# 
# 
# # - ENGLISH
# data=${endata}
# expe=${enexpe}
# 
# echo -- ENGLISH --
# 
# expedir=${expe}/${system}
# mkdir -p ${expedir}
# scores=${expedir}/scores_2H.txt
# bsc=${expedir}/best_scores_2H.txt
# echo  >${scores}
# echo  >${bsc}
# 
# train=${expedir}/train_all-en.tbk
# dev=${expedir}/dev_all-en.tbk
# devraw=${expedir}/dev_all-en.raw
# devref=${expedir}/dev_all-en.brackets
# 
# 
# python projects/multilingual/expe/scripts/union_train.py --inpath ${spdata}/train.tbk ${dedata}/train.tbk ${ptdata}/train.tbk --outpath ${train}
# python projects/multilingual/expe/scripts/union_dev.py --inpath ${spdata} ${dedata} ${ptdata} ${nldata} --outpath ${dev} ${devraw} ${devref}
# 
# bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_v2.sh ${expedir} ${data} ${templates} ${scores} ${train} ${dev} ${devraw} ${devref} ${embeddings} ${dims}
# 
# 
# python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 
# 
# tail -n 3 ${bsc}
# echo
# 











# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# Mix all corpora to make a new training set + embeddings (SOURCE + TARGET)
# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
# 
# templates=projects/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
# dims=projects/multilingual/expe/dimensions_w1400_embed
# 
# system=plurilingual-source+target-train-eduembed_s0s1q0_lr_w1400
# echo ${system}
# 
# embeddings=data/edu_embed_200x7.txt





templates=projects/multilingual/expe/discourse_ttd_wd_s0s1q0_lr.tpl
dims=projects/multilingual/expe/dimensions_w350_embed

system=plurilingual-source+target-train-eduembed_s0s1q0_lr_w350_v2
echo ${system}

embeddings=data/edu_embed_50x7.txt



train=${experiments}/train_all-v2.tbk

# python projects/multilingual/expe/scripts/union_train.py --inpath ${dedata}/train.tbk ${endata}/train.tbk ${spdata}/train.tbk ${ptdata}/train.tbk --outpath ${train}




# - GERMAN
data=${dedata}
expe=${deexpe}
echo -- GERMAN --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}


# Optimize on the dev data for the language
bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 

tail -n 3 ${bsc}
echo




# - BRAZILIAN ALL
data=${ptdata}
expe=${ptexpe}
echo -- PORTUGUESE ALL --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo




# - SPANISH
data=${spdata}
expe=${spexpe}
echo -- SPANISH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_spanish_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}


python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo






# - DUTCH
data=${nldata}
expe=${nlexpe}
echo -- DUTCH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}


# Optimize on the dev data for the language
bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}


python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc}

tail -n 3 ${bsc}
echo




# - ENGLISH
data=${endata}
expe=${enexpe}

echo -- ENGLISH --

expedir=${expe}/${system}
mkdir -p ${expedir}
scores=${expedir}/scores_2H.txt
bsc=${expedir}/best_scores_2H.txt
echo  >${scores}
echo  >${bsc}

# Optimize on the dev data for the language
bash projects/multilingual/expe/scripts/expe_more_hidden_layers_2H_embed.sh ${expedir} ${data} ${templates} ${scores} ${train} ${embeddings} ${dims}

python projects/multilingual/expe/scripts/best_score.py --scores ${scores} >${bsc} 

tail -n 3 ${bsc}
echo


