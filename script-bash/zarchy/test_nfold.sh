
# OUTPUT/EXPE dir 
expedir=$1
mkdir -p ${expedir}
# MODELS dir
models=${expedir}
# DATA dir sandbox/basque-pardi/data/dev_test/
data=$2
# TEMPLATES file
tpls=$3
# SCORE file
scores=$4

# DIR with NFOLD FILES TODO RM
nfold=$5

# DIMENSIONS file
dims=$6
# PARSER SOURCE CODE dir
parser=${7}
# SCRIPT SOURCE CODE dir
src=${8}
# EVAL SOURCE CODE dir
eval=${9}
# EMBEDDINGS file
embeddings=${10}


# DATA sOURCE CODE dir
srcd=projects/basque-pardi/script-data
# TODO check: Forced to give a dev file?
train=${data}/dev.tbk
dev=${data}/dev.tbk
# RAW TEST file (always data from the DATA dir)
rawtest=${data}/test.raw
# REF TEST file
testref=${data}/test.brackets


### parameters
# Best hyper parameters
blr=${11}
bdc=${12}
bbeam=${13}
bh=128

it=10       # iterations
threads=1

tmpdata=${data}tmp/
mkdir -p ${tmpdata}

echo ${parser}

# Train on Dev with the best hyper params and test on test

# Optimizing learning rate (lr), decrease constant (dc), size hidden layers (h)
for lr in ${blr}
do 
for dc in ${bdc}
do
for h in ${bh} # 64 256
do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # for a given set of parameters: train n models
    model=${models}/best_model_2H_h${h}_lr${lr}_dc${dc}
    
    mkdir -p ${model}
    
    
    echo ./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev}
    ./${parser}nnt -i ${it} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt


    # for each model (i.e. the model dumped at each iteration)
    for i in `seq 1 ${it}`
    do 
        # get missing pieces
        for f in encoder ttd embed_dims templates
        do
            cat ${model}/${f} > ${model}/iteration${i}/${f}
        done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in ${bbeam}
        do
            # modifies the model/beam file to choose a custom beamsize
            echo ${b} > ${model}/iteration${i}/beam
            # parse test
            ./${parser}nnp -I ${rawtest} -O ${model}/test_it${i}_beam${b} -m ${model}/iteration${i}
                
            
            # evaluate test output
            python ${eval}eval_parser.py --preds ${model}/test_it${i}_beam${b} --gold ${testref} --params set=test_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores}

            
         done
      done 
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



