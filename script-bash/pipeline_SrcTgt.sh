# Cross-validation procedure: 
## - Repeat X times:
##    - split the dev set into N folds
##    - train N models
##    - get the best set of parameters
##    - train on the whole dev set using the best parameters
##    - compute results on the test set
## - Return an average score over the X runs

now=$(date +"%m_%d_%Y")

# Directory containing the code and data for the project
src=projects/basque-pardi/ #$1


# Source for the code to prepare the data
src_data=${src}/script-data/
# Directory containing the original data 
##data=${src}/data/
# Temp data dir
data=/home/chloe/sandbox/basque-pardi/data/ 
# Output directory for nfold data #$?
data_out=/home/chloe/sandbox/basque-pardi/data/nfold/
mkdir -p ${data_out}



# Source for the code of the discourse parser
#TODO: stil in a different directory!
src_parser=projects/multilingual/hyparse_fork/build/ #$2
# Source for the bash scripts
src_bash=${src}/script-bash/
# Source code for evaluation of the discourse parser
src_eval=${src}/script-eval/ 
# Output directory for models and prediction files
expe_out=/media/chloe/miya/experiments/basque_2018_test_pipeline_${now}/ #$3
mkdir -p ${expe_out}
scores_file=${expe_out}/scores_expe_nfold_${now}.txt
echo ${scores_file}
log_file=${expe_out}/log_file_${now}.txt


# TEMPLATES file
tpls=${src}/templates/discourse_ttd_wd_s0s1q0_lr.tpl
# DIMENSIONS file
dims=${src}/dim/dimensions_w350_embed 
# EMBEDDINGS file
embeddings=${src}/embeddings/monolingual_embeddings_basque/fromFacebookWeb/EDU-embed_cc.eu.300.vec.edu
## embeddings=${src}/embeddings/monolingual_embeddings_basque/fromBasqueTeam/EDU-embed_ElhuyarAndWikiModel_cbow_300_chrNGRAM5_win_5_neg_10.vec.edu
# Number of RUNS
X=2
# Number of FOLDS
N=10
# Max number of ITERATIONS
ITERATIONS=10




# -----------------------------------------------------------------
# PREPARING DATA

# For reproducibility, we fix a seed for the random generator (adding +1 for each run)
SEED=123456


# Iterate over the X runs
#for x in $(seq 1 $X)
#	do
	#	echo -e "\n-------------------------"
	#	echo -e "RUN" $x
	#	CURSEED=$(( ${SEED} + $x ))

	#	# Prepare data for CV
	#	fold_data=${data_out}/fold_run${x}/
	#	mkdir -p ${fold_data}
	#	fold_data_tbk=${fold_data}/tbk-raw/
	#	mkdir -p ${fold_data_tbk}

	#	# -- Split data
	#	echo -e '\n--- SPLIT DATA into', $N, 'folds'
    #	# 1/ split the .brackets, .conll and .parse
    #	python ${src_data}/nfold-hyparse-files.py --const ${data}dev_test/ --dep ${data}dev_test/ --parse ${data}dev_test/ --outpath ${fold_data} --nfold $N --seed ${CURSEED}
    #	# 2/ Generate tbk and raw files for each fold
    #	bash ${src_data}/generate_discourse_data_nfold.sh ${fold_data} ${fold_data_tbk} ${src_data}

    #	# -- Merge: For 1 expe: train = merging all the folds except n ; dev = evaluate on fold n
    #	echo -e '\n--- MERGE training sets'
	#	for n in $(seq 1 $N)
    #		do		
    #    		train=${fold_data}train_${n}.tbk
    #    		python ${src_data}/merge-folds.py --inpath ${fold_data_tbk} --nfold $n --outpath ${train} --ext tbk 
    #		done

	#done


# -----------------------------------------------------------------
# TRAINING MODELS

# TODO: Add learning rate range / dc range / values for h

# Iterate over the X runs
for x in $(seq 1 $X)
	do
        expe_out_run=${expe_out}/run${x}/
        mkdir -p ${expe_out_run}
		echo -e "\nRUN" $x "-- Training"
		# expe_out dir, score file, data_in containing nfold training files, parser source, eval scripts, 
		echo ${expe_out_run} ${scores_file} ${data_out}/fold_run${x}/ ${src_parser} ${src_eval} ${embeddings} ${tpls} ${dims} ${ITERATIONS} ${N} $x
		./${src_bash}/training_nfold.sh ${expe_out_run} ${scores_file} ${data_out}/fold_run${x}/ ${src_parser} ${src_eval} ${embeddings} ${tpls} ${dims} ${ITERATIONS} ${N} $x >${log_file}

	done 
