 


now=$(date +"%m_%d_%Y")

# Directory containing the code and data for the project
src=projects/basque-pardi/ #$1


# Directory containing the original data 
##data=${src}/data/
# Temp data dir
es_data=${src}/data/es/ 


# Source for the code of the discourse parser
#TODO: stil in a different directory!
src_parser=projects/multilingual/hyparse_fork/build/ #$2
# Source for the bash scripts
src_bash=${src}/script-bash/
# Source code for evaluation of the discourse parser
src_eval=${src}/script-eval/ 
# Output directory for models and prediction files
expe_out=/media/chloe/miya/experiments/basque_crossling/basque_models-es_${now}/ #$3
mkdir -p ${expe_out}
scores_file=${expe_out}/scores_expe_es_${now}.txt
echo ${scores_file}
log_file=${expe_out}/log_file_${now}.txt


# TEMPLATES file
tpls=${src}/templates/discourse_ttd_wd_s0s1q0_lr.tpl
# DIMENSIONS file
dims=${src}/dim/dimensions_w350_embed 
# EMBEDDINGS file
embeddings=${src}/embeddings/crosslingual_embeddings/eu-es/EDU-embed_es.vec.edu

# Max number of ITERATIONS
ITERATIONS=10




# TRAIN file (.tbk, to be able to make expe using another train set than the one in data)
train=${es_data}/train.tbk
# DEV file
dev=${es_data}/dev.tbk
# RAW DEV file
rawdev=${es_data}/dev.raw
# REF DEV file
devref=${es_data}/dev.brackets
# RAW TEST file (always data from the DATA dir)
rawtesta=${es_data}/testa.raw
# REF TEST file
testrefa=${es_data}/testa.brackets
# RAW TEST file (always data from the DATA dir)
rawtestb=${es_data}/testb.raw
# REF TEST file
testrefb=${es_data}/testb.brackets


threads=2

# Optimizing learning rate (lr), decrease constant (dc), size hidden layers (h)


for lr in 0.01 0.02
do
    for dc in 1e-5 1e-6 1e-7 0
    do

	for h in 128 64 256
	do
    ((j=j%threads)); ((j++==0)) && wait
    (

    # model name
    model=${expe_out}/model_2H_h${h}_lr${lr}_dc${dc}
    mkdir ${model}
    
    # training command line, log printed in model/trainer_log.txt
    echo ./${src_parser}nnt -i ${ITERATIONS} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev}
    ./${src_parser}nnt -i ${ITERATIONS} -t ${tpls} -m ${model} -l ${lr} -d ${dc} -H ${h} -H ${h} -K ${dims} -L ${embeddings} -a -f 2 ${train} ${dev} 2> ${model}/trainer_log.txt

    # for each model (i.e. the model dumped at each iteration)
    for i in `seq 1 ${ITERATIONS}`
    do 
        # get missing pieces
        for f in encoder ttd embed_dims templates
        do
            cat ${model}/${f} > ${model}/iteration${i}/${f}
        done
        
        # parse with beam = 32 16 8 4 2 1 (larger beam is possible)
        for b in 32 16 8 4 2 1
        do
            # modifies the model/beam file to choose a custom beamsize
            echo ${b} > ${model}/iteration${i}/beam
            # parse dev and test
            ./${src_parser}nnp -I ${rawdev} -O ${model}/dev_it${i}_beam${b} -m ${model}/iteration${i}
            ./${src_parser}nnp -I ${rawtesta} -O ${model}/testa_it${i}_beam${b} -m ${model}/iteration${i}
	    ./${src_parser}nnp -I ${rawtestb} -O ${model}/testb_it${i}_beam${b} -m ${model}/iteration${i}
            
            # evaluate dev and test output
            python ${src_eval}eval_parser.py --preds ${model}/dev_it${i}_beam${b} --gold ${devref} --params set=dev_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores_file}
            #> ${model}/eval_dev_it${i}_beam${b}
            python ${src_eval}eval_parser.py --preds ${model}/testa_it${i}_beam${b} --gold ${testrefa} --params set=testa_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores_file}
            python ${src_eval}eval_parser.py --preds ${model}/testb_it${i}_beam${b} --gold ${testrefb} --params set=testb_lr=${lr}_dc=${dc}_h=${h}_it=${i}_beam=${b} >>${scores_file}
            #> ${model}/eval_test_it${i}_beam${b}

        done
    done
    
    ) &
done
done
done

#Cocophotos: should wait for all jobs to finish before finishing itself
for job in $(jobs -p); do
    wait $job;
done



