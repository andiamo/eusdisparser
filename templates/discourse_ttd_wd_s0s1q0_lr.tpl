#        STACK                                   |   QUEUE
#     s2.t     s1.t            s0.t              |   q0         q1         q2        q3
#              /  \            /  \
#             /    \          /    \
#          s1.l    s1.r     s0.l    s0.r
#
#    t -> top
#    r -> right
#    l -> left
#
#    c  -> non-terminal      (if c is used, the last field is ignored, i.e. s0(t,c,word) and s0(t,c,tag) are equivalents)
#    h  -> head of nonterminal
#         word : word form of head
#         tag  : tag of head            (not used in these templates, as all DU have same tag)




P(1)  = s0(t,h,word)             
P(2)  = s1(t,h,word)                          

P(3)  = s0(l,h,word)             
P(4)  = s0(r,h,word)             
P(5)  = s1(l,h,word)             
P(6)  = s1(r,h,word)             

P(7) = q0(word)

P(8) = s0(t,h,prefp1)
P(9) = s1(t,h,prefp1)

P(10) = s0(l,h,prefp1)
P(11) = s0(r,h,prefp1)
P(12) = s1(l,h,prefp1)
P(13) = s1(r,h,prefp1)

P(14) = q0(prefp1)

P(15) = s0(t,h,prefp2)
P(16) = s1(t,h,prefp2)

P(17) = s0(l,h,prefp2)
P(18) = s0(r,h,prefp2)
P(19) = s1(l,h,prefp2)
P(20) = s1(r,h,prefp2)

P(21) = q0(prefp2)

P(22) = s0(t,h,prefp3)
P(23) = s1(t,h,prefp3)

P(24) = s0(l,h,prefp3)
P(25) = s0(r,h,prefp3)
P(26) = s1(l,h,prefp3)
P(27) = s1(r,h,prefp3)

P(28) = q0(prefp3)

P(29) = s0(t,h,suffp1)
P(30) = s1(t,h,suffp1)

P(31) = s0(l,h,suffp1)
P(32) = s0(r,h,suffp1)
P(33) = s1(l,h,suffp1)
P(34) = s1(r,h,suffp1)

P(35) = q0(suffp1)

P(36) = s0(t,h,len)
P(37) = s1(t,h,len)

P(38) = s0(l,h,len)
P(39) = s0(r,h,len)
P(40) = s1(l,h,len)
P(41) = s1(r,h,len)

P(42) = q0(len)

P(43) = s0(t,h,headp1)
P(44) = s1(t,h,headp1)

P(45) = s0(l,h,headp1)
P(46) = s0(r,h,headp1)
P(47) = s1(l,h,headp1)
P(48) = s1(r,h,headp1)

P(49) = q0(headp1)

P(50) = s0(t,h,headp2)
P(51) = s1(t,h,headp2)

P(52) = s0(l,h,headp2)
P(53) = s0(r,h,headp2)
P(54) = s1(l,h,headp2)
P(55) = s1(r,h,headp2)

P(56) = q0(headp2)

P(57) = s0(t,h,headp3)
P(58) = s1(t,h,headp3)

P(59) = s0(l,h,headp3)
P(60) = s0(r,h,headp3)
P(61) = s1(l,h,headp3)
P(62) = s1(r,h,headp3)

P(63) = q0(headp3)

P(64) = s0(t,h,head)
P(65) = s1(t,h,head)

P(66) = s0(l,h,head)
P(67) = s0(r,h,head)
P(68) = s1(l,h,head)
P(69) = s1(r,h,head)

P(70) = q0(head)

P(71) = s0(t,h,position)
P(72) = s1(t,h,position)

P(73) = s0(l,h,position)
P(74) = s0(r,h,position)
P(75) = s1(l,h,position)
P(76) = s1(r,h,position)

P(77) = q0(position)

P(78) = s0(t,h,numb)
P(79) = s1(t,h,numb)

P(80) = s0(l,h,numb)
P(81) = s0(r,h,numb)
P(82) = s1(l,h,numb)
P(83) = s1(r,h,numb)

P(84) = q0(numb)

P(85) = s0(t,h,perc)
P(86) = s1(t,h,perc)

P(87) = s0(l,h,perc)
P(88) = s0(r,h,perc)
P(89) = s1(l,h,perc)
P(90) = s1(r,h,perc)

P(91) = q0(perc)

P(92) = s0(t,h,money)
P(93) = s1(t,h,money)

P(94) = s0(l,h,money)
P(95) = s0(r,h,money)
P(96) = s1(l,h,money)
P(97) = s1(r,h,money)

P(98) = q0(money)

P(99) = s0(t,h,date)
P(100) = s1(t,h,date)

P(101) = s0(l,h,date)
P(102) = s0(r,h,date)
P(103) = s1(l,h,date)
P(104) = s1(r,h,date)

P(105) = q0(date)

P(106)  = s0(t,c,word)             
P(107)  = s1(t,c,word)             
P(108)  = s2(t,c,word)             

P(109)  = s0(l,c,word)             
P(110)  = s0(r,c,word)             
P(111)  = s1(l,c,word)             
P(112)  = s1(r,c,word)


